"""
Author: Richard Foltyn
"""

import numpy as np

from numba import jit

JIT_OPTIONS = {'nopython': True, 'nogil': True, 'parallel': False}


@jit(**JIT_OPTIONS)
def prob_fill(params, theta, deriv=False):
    """
    Compute job-filling probability for given market tightness theta,
    defined as

        q(theta) = (1 + theta^alpha)^(-1/alpha)

    which is obtained from the matching function

        m(v,u) = (v^-alpha + u^-alpha)^(-1/alpha)

    as q = m(v,u)/v

    Parameters
    ----------
    params : defs.Params
    theta : float or np.ndarray
    deriv : bool, optional
        If true, instead return the derivative dq/dtheta

    Returns
    -------
    float or np.ndarray
    """

    alpha = params.alpha

    q = (1.0 + theta**alpha)**(-1.0/alpha)

    if deriv:
        # Derivative given by
        #   dq/dtheta = - (1+theta^alpha)^(-(1+alpha)/alpha) * theta^(alpha-1)
        #             = - q^(1+alpha) * theta^(alpha-1)
        res = - q**(1.0+alpha) * theta**(alpha-1.0)
    else:
        res = q

    return res


@jit(**JIT_OPTIONS)
def prob_fill_inv(params, q, deriv=False):
    """
    Compute inverse function of q(theta), ie. theta = theta(q), for given
    job-filling probability q.

    Parameters
    ----------
    params : defs.Params
    q : float or np.ndarray
    deriv : bool, optional
        If true, return derivative dtheta(q)/dq instead

    Returns
    ------
    float or np.ndarray
    """

    alpha = params.alpha

    theta = (q**(-alpha) - 1.0)**(1.0/alpha)

    if deriv:
        # Derivative wrt. q is given by
        #   dtheta/dq = - (q^-alpha - 1)^((1-alpha)/alpha) q^(-alpha-1)
        #             = - theta^(1-alpha) * q^(-alpha-1)
        res = - theta**(1.0-alpha) * q**(-alpha-1.0)
    else:
        res = theta

    return res


@jit(**JIT_OPTIONS)
def prob_find(params, theta, deriv=False):
    """
    Compute job-finding probability for given market tightness, given by
        eta = (1 + theta^-alpha)^(-1/alpha)

    which is obtained from the matching function

        m(v,u) = (v^-alpha + u^-alpha)^(-1/alpha)

    as eta = m(v,u)/u

    Parameters
    ----------
    params : defs.Params
    theta : float or np.ndarray
    deriv : bool, optional
        If true, return the derivative deta/dtheta instead

    Returns
    -------
    float or np.ndarray
    """

    alpha = params.alpha

    eta = (1.0 + theta**(-alpha))**(-1.0/alpha)

    if deriv:
        # Derivative is given by
        #   deta/dtheta = (1+theta^-alpha)^(-(1+alpha)/alpha) theta^(-alpha-1)
        #               = eta^(1+alpha) * theta^(-alpha-1)
        res = (eta/theta)**(1.0+alpha)
    else:
        res = eta

    return res


def test_derivs(params):
    """
    Test some functions and derivatives.

    Parameters
    ----------
    params : defs.Params
    """

    import defs

    if isinstance(params, defs.Params):
        params = params.to_numba()

    thetas = np.linspace(1.0e-8, 100.0, 1000)

    eta = prob_find(params, thetas)
    q = prob_fill(params, thetas)

    # We should have eta = q * theta
    assert np.all(np.abs(eta - q * thetas) < 1.0e-12)

    deta_dtheta = prob_find(params, thetas, deriv=True)
    dq_dtheta = prob_fill(params, thetas, deriv=True)

    # Compute deta/dtheta using alternative way
    #   eta = q(theta) * theta
    #   deta/dtheta = dq/dtheta * theta + q(theta)
    x = dq_dtheta * thetas + q
    assert np.all(np.abs(deta_dtheta - x) < 1.0e-12)
