"""
Author: Richard Foltyn
"""

from os.path import join
import pickle

import numpy as np

from defs import Environment, Params
from firms import solve_firm
from funcs import prob_fill, prob_find, test_derivs
from households import solve_hh
from pydynopt.interpolate import interp1d

from pydynopt.plot import PlotMap, DefaultStyle


def plot_firm(env, params, firm):
    """
    Plot solution to firm problem.

    Parameters
    ----------
    env : defs.Environment
    params : defs.Params
    firm : defs.FirmSolution
    """

    # Create numba-compatible instance
    if isinstance(params, Params):
        params = params.to_numba()

    style = DefaultStyle()
    style.aspect = 1.3
    style.cell_size = 5.0
    label_yprod = 'iy={.index:d}'

    # Mapping from data arrays to plot
    pm = PlotMap()
    pm.map_xaxis(dim=1, label='Wage', values=params.firm_wages)
    pm.map_layers(dim=0, label_fmt=label_yprod)

    # Matched firm's value function
    fn = join(env.graphdir, 'firm_J.pdf')
    pm.plot(firm.J, ylabel=r'$J(y,w)$', legend_loc='upper right', style=style,
            outfile=fn)

    # Matched firm's value function derivative
    fn = join(env.graphdir, 'firm_dJdw.pdf')
    pm.plot(firm.dJ_dw, ylabel=r'$d J(y,w)/dw$', legend_loc='upper right',
            style=style, outfile=fn)

    # Market tightness
    thetas = np.copy(firm.thetas)
    thetas[~firm.pfun_search] = np.nan
    fn = join(env.graphdir, 'firm_thetas.pdf')
    pm.plot(thetas, ylabel=r'$\theta(y,w)$', legend_loc='upper right',
            style=style, outfile=fn)

    # Market tightness derivative wrt. wage
    dthetas_dw = np.copy(firm.dthetas_dw)
    dthetas_dw[~firm.pfun_search] = np.nan
    fn = join(env.graphdir, 'firm_dthetas_dw.pdf')
    pm.plot(dthetas_dw, ylabel=r'$d\theta(y,w)/dw$', legend_loc='upper right',
            style=style, outfile=fn)

    # Job-filling probability
    q = np.full_like(thetas, fill_value=np.nan)
    q[firm.pfun_search] = prob_fill(params, thetas[firm.pfun_search])
    fn = join(env.graphdir, 'firm_q.pdf')
    ylabel = r'$q(\theta(y,w)) = q(y,w)$'
    pm.plot(q, ylabel=ylabel, legend_loc='upper right', style=style, outfile=fn)


def plot_hh(env, params, hh, firm):
    """

    Parameters
    ----------
    env : defs.Environment
    params : defs.Params
    hh : defs.HHSolution
    firm : defs.FirmSolution
    """

    # Create numba-compatible instance
    if isinstance(params, Params):
        params = params.to_numba()

    style = DefaultStyle()
    style.aspect = 1.1
    style.cell_size = 4.5
    label_yprod = 'iy={.index:d}'
    label_wage = 'w={.value:.2f}'
    label_assets = 'a={.value:.2f}'

    # === Employed HH ===

    # --- Plot with CoH on x-axis ---

    # Choose 5 uniformly spread wage levels
    Nwages = params.wages.size
    idx_wages = np.arange(0, Nwages, Nwages // 5)

    pm = PlotMap()
    pm.map_xaxis(dim=2, label='Assets', values=params.assets)
    pm.map_columns(dim=1, label_fmt=label_wage, values=params.wages,
                   at_idx=idx_wages, label_loc='lower right')
    pm.map_layers(dim=0, label_fmt=label_yprod)

    # Consumption along CoH dimension
    fn = join(env.graphdir, 'hh_pfun_ce.pdf')
    pm.plot(hh.pfun_ce, ylabel=r'$C^e(y,w,a)$', style=style, outfile=fn)

    # Value function along CoH dimension
    fn = join(env.graphdir, 'hh_ve.pdf')
    pm.plot(hh.Ve, ylabel=r'$V^e(y,w,a)$', legend_loc='center right',
            style=style, outfile=fn)

    # --- Plot with wages on x-axis ---

    # Choose 5 uniformly spread CoH levels
    Nassets = params.assets.size
    idx_assets = np.arange(0, Nassets, Nassets // 5)

    pm = PlotMap()
    pm.map_xaxis(dim=1, label='Wage', values=params.wages)
    pm.map_columns(dim=2, label_fmt=label_assets, values=params.assets,
                   at_idx=idx_assets, label_loc='lower right')
    pm.map_layers(dim=0, label_fmt=label_yprod)

    # Consumption along CoH dimension
    fn = join(env.graphdir, 'hh_pfun_ce_wage.pdf')
    pm.plot(hh.pfun_ce, ylabel=r'$C^e(y,w,a)$', style=style, outfile=fn)

    # Value function along CoH dimension
    fn = join(env.graphdir, 'hh_ve_wage.pdf')
    pm.plot(hh.Ve, ylabel=r'$V^e(y,w,a)$', legend_loc='center right',
            style=style, outfile=fn)

    # --- Derivative dVe/dw ---

    # Numerical derivative dVe/dw
    grad = np.gradient(hh.Ve, params.wages, axis=1)
    data = np.stack((hh.dVe_dw, grad), axis=-1)
    rdiff = np.abs(grad - hh.dVe_dw)/(np.abs(hh.dVe_dw) + 1.0e-10)
    rdiff = np.log10(rdiff)

    labels = ['Analytical', 'Numerical']

    # dVe/dwn along asset dimension
    pm = PlotMap()
    pm.map_xaxis(dim=2, label='Assets', values=params.assets)
    pm.map_columns(dim=1, label_fmt=label_wage, values=params.wages,
                   at_idx=idx_wages, label_loc='lower right')
    pm.map_rows(dim=0, label_fmt=label_yprod)
    pm.map_layers(dim=3, label=labels)

    fn = join(env.graphdir, 'hh_dVe_dw.pdf')
    pm.plot(data, ylabel=r'$dV^e(y,w,a)/dw$', style=style, outfile=fn)

    # Plot log10 abs. rel. diff
    pm = PlotMap()
    pm.map_xaxis(dim=2, label='Assets', values=params.assets)
    pm.map_columns(dim=1, label_fmt=label_wage, values=params.wages,
                   at_idx=idx_wages, label_loc='lower right')
    pm.map_layers(dim=0, label_fmt=label_yprod)
    fn = join(env.graphdir, 'hh_dVe_dw_rdiff.pdf')
    pm.plot(rdiff, ylabel=r'Log10 |rel. diff.|', style=style, outfile=fn)

    # dVe/dw along wage dimension
    pm = PlotMap()
    pm.map_xaxis(dim=1, label='Wage', values=params.wages)
    pm.map_columns(dim=2, label_fmt=label_assets, values=params.assets,
                   at_idx=idx_assets, label_loc='lower right')
    pm.map_rows(dim=0, label_fmt=label_yprod)
    pm.map_layers(dim=3, label=labels)

    fn = join(env.graphdir, 'hh_dVe_dw_wage.pdf')
    pm.plot(data, ylabel=r'$dV^e(y,w,a)/dw$', style=style, outfile=fn)

    # Plot log10 abs. rel. diff
    pm = PlotMap()
    pm.map_xaxis(dim=1, label='Wage', values=params.wages)
    pm.map_columns(dim=2, label_fmt=label_assets, values=params.assets,
                   at_idx=idx_assets, label_loc='lower right')
    pm.map_layers(dim=0, label_fmt=label_yprod)
    fn = join(env.graphdir, 'hh_dVe_dw_wage_rdiff.pdf')
    pm.plot(rdiff, ylabel=r'Log10 |rel. diff.|', style=style, outfile=fn)

    # === Unemployed HH ===

    pm = PlotMap()
    pm.map_xaxis(dim=1, label='Assets', values=params.assets)
    pm.map_layers(dim=0, label_fmt=label_yprod)

    # Consumption
    fn = join(env.graphdir, 'hh_pfun_cu.pdf')
    pm.plot(hh.pfun_cu, ylabel=r'$C^u(y,a)$', style=style, outfile=fn)

    # Wage
    fn = join(env.graphdir, 'hh_pfun_wage.pdf')
    pm.plot(hh.pfun_wage, ylabel=r'$Wage^u(y,a)$', style=style, outfile=fn)

    # Job-finding probability
    thetas = np.empty_like(hh.pfun_wage)
    for iy in range(params.yprod.size):
        thetas[iy] = interp1d(hh.pfun_wage[iy], params.firm_wages, firm.thetas[iy])
    etas = prob_find(params, thetas)
    fn = join(env.graphdir, 'hh_prob_find.pdf')
    pm.plot(etas, ylabel=r'$\eta(\theta(y,a))$', style=style, outfile=fn)

    # Derivative of job-finding probability at optimally chosen wage
    detas_dthetas = prob_find(params, thetas, deriv=True)
    dthetas_dw = np.empty_like(detas_dthetas)
    for iy in range(params.yprod.size):
        dthetas_dw[iy] = interp1d(hh.pfun_wage[iy], params.firm_wages, firm.dthetas_dw[iy])
    detas_dw = detas_dthetas * dthetas_dw
    fn = join(env.graphdir, 'hh_deta_dw.pdf')
    pm.plot(detas_dw, ylabel=r'$\partial \eta(\theta(y,w))/\partial w$', style=style, outfile=fn)

    # Value function
    fn = join(env.graphdir, 'hh_vu.pdf')
    pm.plot(hh.Vu, ylabel=r'$V^u(y,a)$', style=style, legend_loc='center right',
            outfile=fn)


def solve_all(params):
    """
    Solve firm and household problems.

    Parameters
    ----------
    params : defs.Params

    Returns
    -------
    firm : defs.FirmSolution
    hh : defs.HHSolution
    """

    # Solve matched and searching firm's problem
    firm = solve_firm(params)
    hh = solve_hh(params, firm)

    return firm, hh


def main(env):
    """
    
    Parameters
    ----------
    env : defs.Environment
    """

    load = False
    label = 'model6'
    fn_res = join(env.resultdir, 'result_{:s}.bin'.format(label))

    # Store graphs in model-specific folders
    env.graphdir = join(env.graphdir, label)
    # Make sure folders exist
    env.setup()

    if not load:
        # Solve PE model
        params = Params()
        params.create_derived()

        firm, hh = solve_all(params)

        # Store results
        with open(fn_res, 'wb') as f:
            pickle.dump((params, firm, hh), f)
    else:
        # Load result from file
        with open(fn_res, 'rb') as f:
            params, firm, hh = pickle.load(f)

    # --- Plot results ---

    plot_firm(env, params, firm)
    plot_hh(env, params, hh, firm)


if __name__ == '__main__':
    env = Environment()
    main(env)
