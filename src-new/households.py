"""
Author: Richard Foltyn
"""


import numpy as np
from time import perf_counter

from defs import HHSolution, Params
from funcs import prob_find
from pydynopt.arrays import powerspace
from pydynopt.interpolate import interp1d, interp2d_locate, interp2d_eval, \
    interp2d
from pydynopt.optimize import newton_bisect

import matplotlib.pyplot as plt

JIT_OPTIONS = {'nopython': True, 'nogil': True, 'parallel': False}

use_numba = True

if use_numba:
    from numba import jit
else:
    # Create no-op jit decorator
    def jit(*args, **kwargs):
        def decorate(func):
            return func
        return decorate


def solve_hh(params, firm):
    """
    Solve employed and unemployed HH problems.

    Parameters
    ----------
    params : defs.Params
    firm : defs.FirmSolution

    Returns
    -------
    hh : defs.HHSolution
    """

    # Create numba-compatible instance
    firm = firm.to_numba()

    res = HHSolution()

    # (Re)create wage grid such that in spans the entire rage of wages
    # offered in all sub-markets
    wage_min = np.floor(params.ui_ben * 100.0) / 100.0
    wage_max = 0.0
    for iy in range(params.yprod.size):
        x = np.amax(params.firm_wages[firm.pfun_search[iy]])
        wage_max = max(x, wage_max)

    wage_max = np.ceil(wage_max * 100.0)/100.0

    params.wages[:] = powerspace(wage_min, wage_max, params.wages_N, 1.5)

    # Create numba-compatible instance
    if isinstance(params, Params):
        params = params.to_numba()

    # Call implementation routine to solve HHP
    solve_hh_impl(params, firm, res)

    return res


def solve_hh_impl(params, firm, res, maxiter=1000, tol=1.0e-8):
    """
    Implements solver for problem of employed and unemployed households.

    This function returns the solutions to the employed and unemployed HH's
    problems.

    The employed HH solves

        Ve(y,x,w) = max_{c,a'} { u(c) + beta * (1-delta) * E[Ve(y',a',w)|y]
                        + beta * delta * E[Vu(y',a')|y] }
            s.t.    c + a' = x
                    a' = (1+r)a' + (1-tau)w
                    a' = (1+r)a' + (1-tau)b

        where delta is the exogenous separate probability.

    The unemployed HH solves

        Vu(y,x) = max_{c,a',w} {u(c) + beta * eta(y,w) * E[Ve(y',a',w)|y]
                    + beta * (1-eta(y,w)) E[Vu(y',a')|y] }

            s.t. the same constraints as the employed HH.

        The job-finding rate eta is a function of the chosen wage vie
        the market tightness theta,

            eta(y,w) = eta(theta(y,w))

        where the mapping theta(y,w) is equilibrium solution to a non-matched
        firm's problem.

        Note that this implies a feasible range for w, w in [wmin(y), wmax(y)]
        since some sub-markets (theta(y,w),w) will not exist if no firm
        would ever want to post a vacancy there.

    Parameters
    ----------
    params : defs.Params
    firm : defs.FirmSolution
    res : defs.HHSolution
        Result object used to store converged value/policy functions
    maxiter : int
    tol : float

    Returns
    -------
    res : defs.HHSolution
    """

    Nyprod, Nwages, Nassets = params.yprod.size, params.wages.size, params.assets.size
    Nsav = params.sav.size

    # --- Extended continuation asset grid ---

    # Grid  for extrapolating continuation values
    Nadd = Nassets // 2
    if Nadd > 0:
        # Find powerspace() parameters such that the distance between
        # first two grid points on extended grid is the same as distance
        # between two terminal points on regular grid.
        amin = params.assets[-1]
        amax = params.assets[-1] * 2.0
        da = params.assets[-1] - params.assets[-2]
        dex = amax - amin
        xp = np.log(da/dex) / np.log(1.0/(Nadd + 1))
        assets_ex = powerspace(amin, amax, Nadd+2, xp)
        # Skip first two points which are overlapping with regular grid
        assets_ex = np.hstack((params.assets, assets_ex[2:]))
    else:
        assets_ex = params.assets

    Nassets_ex = assets_ex.size

    pfun_ce = np.empty((Nyprod, Nwages, Nassets))
    pfun_cu = np.empty((Nyprod, Nassets))
    pfun_wage = np.zeros((Nyprod, Nassets))

    # Initialize consumption policy such that HH consumes everything half
    # of its CoH
    r, tau, ui_ben = params.r, params.tau, params.ui_ben
    pfun_ce[...] = params.assets.reshape((1, 1, -1)) * (1.0+r) \
                   + (1.0-tau) * params.wages.reshape((1, -1, 1))
    pfun_cu[...] = params.assets.reshape((1, -1)) * (1.0+r) + (1.0-tau) * ui_ben

    pfun_ce_upd = np.empty_like(pfun_ce)
    pfun_cu_upd = np.empty_like(pfun_cu)
    pfun_wage_upd = np.empty_like(pfun_wage)

    # Define on extrapolating CoH grid
    Ve = np.zeros((Nyprod, Nwages, Nassets_ex))
    Vu = np.zeros((Nyprod, Nassets_ex))

    Ve_upd = np.empty_like(Ve)
    Vu_upd = np.empty_like(Vu)

    dVe_dw = np.zeros((Nyprod, Nwages, Nassets_ex))
    dVe_dw_upd = np.empty_like(dVe_dw)

    pfun_upd = np.empty(Nassets)

    rhs_ee = np.empty(Nsav)
    cons_sav = np.empty(Nsav)
    coh_sav = np.empty(Nsav)
    assets_sav = np.empty(Nsav)
    # Implied beginning-of-period CoH for given wage and beginning-of-period
    # assets
    coh = np.empty(Nassets)

    # Cache optimal wages found at previous iteration, to be used as
    # starting point for root finder.
    wage_sav = np.full((Nyprod, Nsav), fill_value=params.ui_ben * 1.01)
    thetas_sav = np.empty(Nsav)
    etas_sav = np.empty(Nsav)

    t0 = perf_counter()

    for it in range(maxiter):

        # === Employed HH ===

        # --- Optimal savings ---

        for iy in range(Nyprod):
            for iw in range(Nwages):

                wage = params.wages[iw]

                # 1. Compute RHS of EE
                compute_EE_RHS_empl(params, iy, iw, pfun_ce, pfun_cu, rhs_ee)

                # 2. Invert EE to obtain C^e(iy,wage,sav), a function of savings
                cons_sav[:] = rhs_ee**(-1.0/params.gamma)

                # 3. Obtain implied beginning-of-period CoH
                coh_sav[:] = cons_sav + params.sav

                # 4. Implied beginning-of-period assets
                assets_sav[:] = (coh_sav - (1.0-tau) * wage) / (1.0 + r)

                # Distinguish two cases:
                #   (1) assets_sav[0] < params.assets[0]: HH saves positive
                #       amount for any asset level on the grid. Simple
                #       interpolation is sufficient.
                #   (2) assets_sav[0] >= params.assets[0]: HH does not save
                #       anything for some initial elements on asset grid,
                #       need to fix cons. policy *after* interpolation.

                # 5. Interpolate onto beginning-of-period CoH
                interp1d(params.assets, assets_sav, cons_sav, out=pfun_upd)

                # 6. Fix asset levels where HH does not save
                imin = np.sum(params.assets < assets_sav[0])
                pfun_upd[:imin] = (1.0 + r) * params.assets[:imin] + (1.0-tau) * wage

                # Beginning-of-period CoH corresponding to beginning-of-period
                # asset grid and given wage
                coh[:] = (1.0 + r) * params.assets + (1.0 - tau) * wage
                assert np.all(coh >= pfun_upd)
                assert np.all(pfun_upd > 0.0)
                assert np.all(np.diff(pfun_upd) >= 0.0)

                # 7. Store updated policy function
                pfun_ce_upd[iy, iw] = pfun_upd

        # --- Update value function and its derivative ---

        for iy in range(Nyprod):
            for iw in range(Nwages):
                update_Ve(params, iy, iw, assets_ex, pfun_ce_upd, Ve, Vu, dVe_dw,
                          Ve_upd[iy, iw], dVe_dw_upd[iy, iw])

        # === Unemployed HH ===

        for iy in range(Nyprod):

            # --- Optimal wage ---

            for iap in range(Nsav):

                # Use previous optimal wage as starting point
                wage = wage_sav[iy, iap]
                sav = params.sav[iap]
                wage = wage_policy(params, iy, sav, assets_ex, wage, pfun_ce, Ve,
                                   Vu, dVe_dw, firm)

                wage_sav[iy, iap] = wage

            # Recover market tightness for chosen wages
            interp1d(wage_sav[iy], params.firm_wages, firm.thetas[iy], out=thetas_sav)
            # Recover job-finding rates for chosen wages
            etas_sav[:] = prob_find(params, thetas_sav)

            assert np.all(etas_sav >= 0.0)
            assert np.all(etas_sav <= 1.0)

            # --- Optimal savings given wage ---

            # 1. Compute EE RHS
            compute_EE_RHS_unempl(params, iy, wage_sav[iy], etas_sav, pfun_ce, pfun_cu, rhs_ee)

            # 2. Invert EE to obtain C^u(iy,sav), a function of savings
            cons_sav[:] = rhs_ee**(-1.0/params.gamma)

            # 3. Obtain implied beginning-of-period CoH
            coh_sav[:] = cons_sav[:] + params.sav

            # Need non-decreasing CoH for EGM to work
            assert np.all(np.diff(coh_sav) >= 0.0)

            # 4. Implied beginning-of-period assets
            assets_sav[:] = (coh_sav - (1.0 - tau)*ui_ben)/(1.0 + r)

            # 5. Interpolate onto beginning-of-period CoH
            interp1d(params.assets, assets_sav, cons_sav, out=pfun_upd)

            # 6. Fix asset levels where HH does not save
            imin = np.sum(params.assets < assets_sav[0])
            pfun_upd[:imin] = (1.0 + r)*params.assets[:imin] + (1.0 - tau)*ui_ben

            # Beginning-of-period CoH corresponding to beginning-of-period
            # asset grid and ui benefits
            coh[:] = (1.0 + r)*params.assets + (1.0 - tau)*ui_ben
            assert np.all(pfun_upd <= coh)
            assert np.all(pfun_upd > 0.0)
            assert np.all(np.diff(pfun_upd) >= 0.0)

            # 8. Store updated policy function
            pfun_cu_upd[iy] = pfun_upd

            # 9. Interpolate wage into beginning-of-period CAH grid
            interp1d(params.assets, assets_sav, wage_sav[iy], out=pfun_upd)

            # 10. Wage is constant for all CAH levels where HH does not save
            # since all continuation for zero savings are the same, and hence
            # the wage FOC will give the same result.
            pfun_upd[0:imin] = wage_sav[iy, 0]

            assert np.all(pfun_upd > 0.0)

            # Store updated wage policy
            pfun_wage_upd[iy] = pfun_upd

        # --- Update wage policy function ---

        # pfun_wage_upd2 = np.empty_like(pfun_wage_upd)
        #
        # for iy in range(Nyprod):
        #     for ia, a in enumerate(params.assets):
        #         # Use previous wage as a starting point
        #         wage = pfun_wage[iy, ia]
        #         # Savings policy
        #         sav = (1.0 + r) * a + (1.0 - tau) * ui_ben - pfun_cu[iy, ia]
        #         wage = wage_policy(params, iy, sav, assets_ex, wage, pfun_ce,
        #                            Ve, Vu, dVe_dw, firm)
        #         pfun_wage_upd2[iy, ia] = wage

        # --- Update value function ---

        for iy in range(Nyprod):
            update_Vu(params, iy, assets_ex, pfun_cu_upd, pfun_wage_upd, Ve, Vu,
                      firm, Vu_upd[iy])

        # --- Check convergence ---

        diff_Ve = np.amax(np.abs(Ve - Ve_upd))
        diff_Vu = np.amax(np.abs(Vu - Vu_upd))
        diff_dVe_dw = np.amax(np.abs(dVe_dw - dVe_dw_upd))
        diff_pfun_ce = np.amax(np.abs(pfun_ce - pfun_ce_upd))
        diff_pfun_cu = np.amax(np.abs(pfun_cu - pfun_cu_upd))
        diff_pfun_wage = np.amax(np.abs(pfun_wage - pfun_wage_upd))

        diff = (diff_Ve, diff_Vu, diff_dVe_dw, diff_pfun_ce, diff_pfun_cu, diff_pfun_wage)

        if np.all(np.array(diff) < tol):
            dt = perf_counter() - t0
            msg = 'HH: problem converged after {:d} iterations ({:.2f} sec)'.format(it, dt)
            print(msg)
            break
        elif it % 50 == 0:
            msg = 'HH: Iteration {:4d}, max. d(x) = {:.4e}'.format(it, np.amax(diff))
            print(msg)

        # Swap arrays for next iteration
        Ve, Ve_upd = Ve_upd, Ve
        Vu, Vu_upd = Vu_upd, Vu
        dVe_dw, dVe_dw_upd = dVe_dw_upd, dVe_dw
        pfun_ce, pfun_ce_upd = pfun_ce_upd, pfun_ce
        pfun_cu, pfun_cu_upd = pfun_cu_upd, pfun_cu
        pfun_wage, pfun_wage_upd = pfun_wage_upd, pfun_wage

    else:
        msg = 'HH: Failed to converge'
        print(msg)

    # Discard extended CoH elements in result object arrays
    # Recall that the first Ncoh elements on both grids are identical, so
    # no interpolation is required.
    res.Ve = np.ascontiguousarray(Ve_upd[:, :, :Nassets])
    res.Vu = np.ascontiguousarray(Vu_upd[:, :Nassets])
    res.dVe_dw = np.ascontiguousarray(dVe_dw_upd[:, :, :Nassets])
    res.pfun_cu = pfun_cu_upd
    res.pfun_ce = pfun_ce_upd
    res.pfun_wage = pfun_wage_upd

    return res


@jit(**JIT_OPTIONS)
def compute_EE_RHS_empl(params, iy, iw, pfun_ce, pfun_cu, rhs):
    """
    Compute RHS of the EE of employed households for given state (iy,iw).

    The EE for the employed HH is given by

        u'(c) = beta * (1-delta) E[u'(Ce(y',ae',w))*(1+r)|y]
              + beta * delta * E[u'(Cu(y',au'))*(1+r)|y]

    Parameters
    ----------
    params : defs.Params
    iy : int
    iw : int
    pfun_ce : np.ndarray
    pfun_cu : np.ndarray
    rhs : np.ndarray
        Array to store RHS of EE, shaped [Nyprod,Nwages,Nsav]

    Returns
    -------
    rhs : np.ndarrasy
    """

    Nyprod, Nsav = params.yprod.size, params.sav.size

    beta, gamma, delta = params.beta, params.gamma, params.delta
    r = params.r

    c_to = np.empty(Nsav)
    uc_to = np.empty(Nsav)

    rhs[...] = 0.0

    # Iterate over all future y'
    for iy_to in range(Nyprod):

        prob_y = params.yprod_transm[iy, iy_to]
        if prob_y == 0.0:
            continue

        # Compute expectation E[u'(c')|y,w] for all a'

        # Employed tomorrow
        interp1d(params.sav, params.assets, pfun_ce[iy_to, iw], out=c_to)
        uc_to[:] = c_to**(-gamma)
        rhs += prob_y * (1.0 - delta) * uc_to

        # Unemployed tomorrow
        interp1d(params.sav, params.assets, pfun_cu[iy_to], out=c_to)
        uc_to[:] = c_to**(-gamma)
        rhs += prob_y * delta * uc_to

    # Rescale by beta * (1+r) to complete EE RHS
    rhs *= beta * (1.0 + r)

    return rhs


@jit(**JIT_OPTIONS)
def compute_EE_RHS_unempl(params, iy, wages, etas, pfun_ce, pfun_cu, rhs):
    """
    Compute RHS of the EE of unemployed households for given state iy.

    The EE for the employed HH is given by

        u'(c) = beta * (1-eta) E[u'(Ce(y',a',w))*(1+r)|y]
              + beta * eta * E[u'(Cu(y',a'))*(1+r)|y]

    Parameters
    ----------
    params : defs.Params
    iy : int
    wages : np.ndarray
        Optiomal wage level for each exogenous savings level
    etas : np.ndarray
        Implied job-finding probabilities for each chosen wage.
    pfun_ce : np.ndarray
    pfun_cu : np.ndarray
    rhs : np.ndarray
    """

    Nyprod, Nsav = params.yprod.size, params.sav.size

    beta, gamma = params.beta, params.gamma
    r = params.r

    c_to = np.empty(Nsav)
    uc_to = np.empty(Nsav)

    rhs[...] = 0.0

    # Iterate over all future y'
    for iy_to in range(Nyprod):

        prob_y = params.yprod_transm[iy, iy_to]
        if prob_y == 0.0:
            continue

        # Compute expectation E[u'(c')|y,w] for all a'

        # Employed tomorrow
        interp2d(wages, params.sav, params.wages, params.assets, pfun_ce[iy_to], out=c_to)
        uc_to[:] = c_to**(-gamma)
        rhs += prob_y * etas * uc_to

        # Unemployed tomorrow
        interp1d(params.sav, params.assets, pfun_cu[iy_to], out=c_to)
        uc_to[:] = c_to**(-gamma)
        rhs += prob_y * (1.0 - etas) * uc_to

    # Rescale by beta * (1+r) to complete EE RHS
    rhs *= beta * (1.0 + r)

    return rhs


@jit(**JIT_OPTIONS)
def update_Ve(params, iy, iw, assets_ex, pfun_ce, Ve, Vu, dVe_dw, Ve_upd, dVe_dw_upd):
    """
    Update the guesses for the value function of the employed HH, Ve, and
    its derivative dVe/dw, for a given state (iy,iw).

    The derivative dVe/dw follows from the envelope condition and is given by

        dVe(y,a,w)/dw = u'(C^e(y,a,w)) * (1-tau)
                        + beta * (1-delta) * E[dVe(y',a',w)/dw|y]


    Parameters
    ----------
    params : defs.Params
    iy : int
        Idiosyncratic productivity index.
    iw : int
        Index on wage grid.
    assets_ex : np.ndarray
        Extended assets on which value functions and their derivatives
        are defined.
    pfun_ce : np.ndarray
        Consumption policy of employed HH
    Ve : np.ndarray
        Current guess for employed HH's value function
    Vu : np.ndarray
        Current guess for unemployed HH's value function
    dVe_dw : np.ndarray
        Current guess for employed HH's value function derivative dVe/dw
    Ve_upd : np.ndarray
        Array to which updated value function of employed will be written
    dVe_dw_upd : np.ndarray
        Array to which updated derivative of value function of employed will
        be written
    """

    Nyprod, Nassets_ex = params.yprod.size, assets_ex.size

    beta, gamma, delta = params.beta, params.gamma, params.delta
    r, tau = params.r, params.tau

    wage = params.wages[iw]

    # CoH today, defined on extended grid
    coh_ex = assets_ex * (1.0 + r) + (1.0 - tau) * wage

    # Consumption today, inter/extrapolated onto extended CoH grid
    cons = interp1d(assets_ex, params.assets, pfun_ce[iy, iw])

    # savings today, defined on extended CoH grid
    a_to = coh_ex - cons

    assert np.all(a_to >= 0.0)

    dVe_dw_upd[...] = 0.0
    Ve_upd[...] = 0.0

    # Pre-allocate some working arrays for loop below
    work = np.empty(Nassets_ex)

    for iy_to in range(Nyprod):

        prob_y = params.yprod_transm[iy, iy_to]
        if prob_y == 0.0:
            continue

        # --- Unemployed tomorrow ---

        # Value of unemployed tomorrow
        interp1d(a_to, assets_ex, Vu[iy_to], out=work)

        # Add to continuation value
        Ve_upd += delta * prob_y * work

        # --- Employed tomorrow ---

        # Value of employed tomorrow
        interp1d(a_to, assets_ex, Ve[iy_to, iw], out=work)

        # Add to continuation value
        Ve_upd += (1.0 - delta) * prob_y * work

        # E[dVe(...)/dw|y,w] for all a'
        interp1d(a_to, assets_ex, dVe_dw[iy_to, iw], out=work)
        dVe_dw_upd += prob_y * work

    # Discounted continuation value
    Ve_upd *= beta

    # Add current-period utility to get
    #   Ve = u(c) + beta * E[V']
    if gamma == 1.0:
        work[:] = np.log(cons)
    else:
        work[:] = cons**(1.0 - gamma) / (1.0 - gamma)

    Ve_upd += work

    # Complete expression, rescaling by beta * (1-delta)
    dVe_dw_upd *= beta * (1.0 - delta)
    # Add current-period MU
    dVe_dw_upd += cons**(-gamma) * (1.0 - tau)


@jit(**JIT_OPTIONS)
def update_Vu(params, iy, assets_ex, pfun_cu, pfun_wage, Ve, Vu, firm, Vu_upd):
    """
    Update the guess for the unemployed HH's value function Vu given its
    current value.

    Parameters
    ----------
    params : defs.Params
    iy : int
        Idiosyncratic productivity index.
    assets_ex : np.ndarray
        Extended assets on which value functions and their derivatives
        are defined.
    pfun_cu : np.ndarray
        Consumption policy function of unemployed HH
    pfun_wage : np.ndarray
        Wage policy function of unemployed HH
    Ve : np.ndarray
        Current guess for employed HH's value function
    Vu : np.ndarray
        Current guess for unemployed HH's value function
    firm : defs.FirmSolution
    Vu_upd : np.ndarray
        Array to which updated value function of employed will be written
    """

    Nyprod, Nassets_ex = params.yprod.size, assets_ex.size

    beta, gamma, delta = params.beta, params.gamma, params.delta
    r, tau, ui_ben = params.r, params.tau, params.ui_ben

    # Extrapolate wage onto extended assets grid (this is somewhat shaky)
    wages = interp1d(assets_ex, params.assets, pfun_wage[iy])
    # Obtain thetas from firm problem
    thetas = interp1d(wages, params.firm_wages, firm.thetas[iy])
    # Implied job-finding probabilities
    etas = prob_find(params, thetas)
    assert np.all(etas >= 0.0) and np.all(etas <= 1.0)

    # CoH today, defined on extended grid
    coh_ex = assets_ex * (1.0 + r) + (1.0 - tau) * ui_ben

    # Consumption today, inter/extrapolated onto extended CoH grid
    cons = interp1d(assets_ex, params.assets, pfun_cu[iy])

    # savings today, defined on extended CoH grid
    a_to = coh_ex - cons

    assert np.all(a_to >= 0.0)

    Vu_upd[...] = 0.0

    # Pre-allocate some working arrays for loop below
    work = np.empty(Nassets_ex)

    for iy_to in range(Nyprod):

        prob_y = params.yprod_transm[iy, iy_to]
        if prob_y == 0.0:
            continue

        # --- Unemployed tomorrow ---

        # Value of unemployed tomorrow
        interp1d(a_to, assets_ex, Vu[iy_to], out=work)

        # Add to continuation value
        Vu_upd += (1.0 - etas) * prob_y * work

        # --- Employed tomorrow ---

        # Value of employed tomorrow
        interp2d(wages, a_to, params.wages, assets_ex, Ve[iy_to], out=work)

        # Add to continuation value
        Vu_upd += etas * prob_y * work

    # Discounted continuation value
    Vu_upd *= beta

    # Add current-period utility to get
    #   Vu = u(c) + beta * E[V']
    if gamma == 1.0:
        work[:] = np.log(cons)
    else:
        work[:] = cons**(1.0 - gamma) / (1.0 - gamma)

    Vu_upd += work


@jit(**JIT_OPTIONS)
def wage_policy(params, iy, sav, assets_ex, wage, pfun_ce, Ve, Vu, dVe_dw, firm):
    """
    Compute optimal wage for unemployed HH given its productivity y and its
    savings level a'.

    Parameters
    ----------
    params : defs.Params
    iy : int
    sav : float
        Savings level
    assets_ex : np.ndarray
        Extended cash-on-hand on which value functions and their derivatives
        are defined.
    wage : float
        Initial guess for wage
    pfun_ce : np.ndarray
    Ve : np.ndarray
    Vu : np.ndarray
    dVe_dw : np.ndarray
    firm : defs.FirmSolution

    Returns
    -------
    wage : float
    """

    # Initial guess for scaling parameter
    scale = 1.0

    # Tuple of args passed to objective
    args = (params, iy, sav, assets_ex, pfun_ce, Ve, Vu, dVe_dw, firm, scale)

    # Find interval for feasible wage choice. Too high wages will not
    # be feasible since the sub-market will not exist due to negative
    # vacancy value.

    wmin = params.firm_wages[0]
    wmax = np.amax(params.firm_wages[firm.pfun_search[iy]])

    # Check for corner solutions
    fxlb = wage_foc(wmin, *args)
    fxub = wage_foc(wmax, *args)

    # Check whether this is monotonically decreasing
    # xx = np.linspace(wmin, wmax, 100)
    # fxx = np.array([wage_foc(wi, *args) for wi in xx])

    # Pretend that f() is monotonically decreasing in wage and check
    # for corner solutions. Anecdotal inspection shows that this is not
    # true towards the end of the wage range, but as long as this does not
    # create multiple roots, we ignore it for now.
    if fxub >= 0.0:
        # f() >= 0.0 for entire range, so optimum must be at upper bound
        # (implied by Lagrange multiplier on w <= wmax!)
        wage = wmax
        return wage
    elif fxlb <= 0.0:
        # f() <= 0.0 for entire wage range, so optimum must be at lower bound.
        wage = wmin
        return wage
    elif fxlb * fxub > 0.0:
        raise RuntimeError('No bracketing interval for wage FOC')

    if wage < wmin or wage > wmax:
        wage = (wmin + wmax) / 2.0

    # Update scaling to value at initial guess
    f0 = wage_foc(wage, *args)
    scale = max(np.abs(f0), 1.0e-6)
    # Update args
    args = (params, iy, sav, assets_ex, pfun_ce, Ve, Vu, dVe_dw, firm, scale)

    root, res = newton_bisect(wage_foc, wage, wmin, wmax, args=args, jac=False,
                              maxiter=50, full_output=True)

    wage = root

    return wage


@jit(**JIT_OPTIONS)
def wage_foc(wage, params, iy, sav, assets_ex, pfun_ce, Ve, Vu, dVe_dw, firm,
             scale=1.0):
    """
    Evaluate the wage FOC of an unemployed household, given by
        f(wage) = deta/dwage * E[Ve(y',a',w)|y]
                    + eta * E[dVe(y',a',w)/dw |y]
                    - deta/dwage * E[Vu(y',a')|y]
                = deta/dwage * (E[Ve(y',a',w)|y] - E[Vu(y',a')|y])
                    +  eta * E[dVe(y',a',w)/dw |y]

    where
        eta = eta(theta(y,wage))
        deta/dwage = deta/dtheta * dtheta/dwage

    We know that
        dtheta/dwage < 0 (from the firm problem)
        deta/dtheta > 0 (analytical derivative)
    and hence deta/dwage < 0

    Parameters
    ----------
    wage : float
    params : defs.Params
    iy : int
    sav : float
        Savings level
    assets_ex : np.ndarray
        Extended cash-on-hand on which value functions and their derivatives
        are defined.
    pfun_ce : np.ndarray
    Ve : np.ndarray
    Vu : np.ndarray
    dVe_dw : np.ndarray
    firm : defs.FirmSolution
    scale : float, optional
        Value by which objective should be rescaled

    Returns
    -------
    fx : float
        Wage FOC evaluated at given wage level
    """

    Nyprod, Nassets = params.yprod.size, params.assets.size

    # theta(iy,w) and dtheta/dw follow from firm solution
    dtheta_dw = interp1d(wage, params.firm_wages, firm.dthetas_dw[iy])
    theta = interp1d(wage, params.firm_wages, firm.thetas[iy])
    eta = prob_find(params, theta)
    deta_dtheta = prob_find(params, theta, deriv=True)
    deta_dw = deta_dtheta * dtheta_dw

    assert 0.0 <= eta <= 1.0

    # Savings level
    a_to = sav

    # Stores final FOC (ignoring beta, since it does not effect the root)
    foc = 0.0

    idx_e = np.zeros((2, ), dtype=np.int64)
    weight_e = np.zeros((2, ), dtype=np.float64)

    for iy_to in range(Nyprod):
        prob_y = params.yprod_transm[iy, iy_to]
        if prob_y == 0.0:
            continue

        # Compute parts of FOC conditional on y' transition

        # Find interpolating indices for 2d interpolation on (wage, assets)
        interp2d_locate(wage, a_to, params.wages, assets_ex,
                        ilb=idx_e, index_out=idx_e, weight_out=weight_e)

        # Ve(y',a',w)
        Ve_to = interp2d_eval(idx_e, weight_e, Ve[iy_to])

        # dVe(y',a',w)/dw
        dVe_dw_to = interp2d_eval(idx_e, weight_e, dVe_dw[iy_to])

        # Vu(y',a')
        Vu_to = interp1d(a_to, assets_ex, Vu[iy_to])

        dV = Ve_to - Vu_to
        foc_y = eta * dVe_dw_to + deta_dw * dV

        # Accumulate FOC
        foc += prob_y * foc_y

    foc /= scale

    return foc


