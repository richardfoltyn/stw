"""
Author: Richard Foltyn
"""

import numpy as np

from defs import FirmSolution
from funcs import prob_fill_inv, prob_fill


def solve_firm(params):
    """
    Compute the solutions for the matched and non-matched firm.

    Parameters
    ----------
    params : defs.Params

    Returns
    -------
    res : defs.FirmSolution
    """

    res = FirmSolution()

    # Create numba-fied Params instance
    params_nb = params.to_numba()

    # Matched firm, can be computed independent of non-matched firm
    J, dJ_dw = solve_matched(params_nb)
    # Non-matched firm, needs matched firm solution as input.
    thetas, dthetas_dw, pfun_search = solve_search(params_nb, J, dJ_dw)

    # Find max. equilibrium wage across all productivity levels
    wage_max = 0.0
    for iy in range(params.yprod.size):
        x = np.amax(params.firm_wages[pfun_search[iy]])
        wage_max = max(x, wage_max)

    # Round up to nearest 1/100
    wage_max = np.ceil(wage_max * 100.0) / 100.0

    # Update wage grid, solve problem again
    # We perform the update on the Params instance and recreate the numba-fied
    # object to prevent them from getting out of sync.
    wage_min, N = params.firm_wages_min, params.firm_wages_N
    params.firm_wages_max = wage_max
    params.firm_wages[:] = np.linspace(wage_min, wage_max, N)

    params_nb = params.to_numba()
    J, dJ_dw = solve_matched(params_nb)
    thetas, dthetas_dw, pfun_search = solve_search(params_nb, J, dJ_dw)

    res.J = J
    res.dJ_dw = dJ_dw
    res.thetas = thetas
    res.dthetas_dw = dthetas_dw
    res.pfun_search = pfun_search

    return res


def solve_matched(params, maxiter=10000, tol=1.0e-10):
    """
    Solve for matched firm's value function and its derivative.

    Firm with ongoing match, exogenous separation probability delta (using
    value of unmatched firm V(y,w) = 0) has value function defined by

        J(y,x,w) = max_h { h(y-w) - 0.5*xi*(1-h)^2 + (1-delta)/(1+r) * E[J(y',x',w)|y]}

    Currenty, we omit the dependence on a, and skip the maximization problem,
    so we have:

        J(y,w) = h(y-w) + (1-delta)/(1+r) * E[J(y',w)|y]

    The derivative wrt. w is then given by
        dJ(y,w)/dw = -h + (1-delta)/(1+r) * E[dJ(y',w)/dw|y]

    Parameters
    ----------
    params : defs.Params
    maxiter : int, optional
        Max. number of iterations
    tol : float, optional
        Convergence tolerance

    Returns
    -------
    J : np.ndarray
        Value function of matched firm, shaped [Nyprod,Nwages]
    dJdw : np.ndarray
        Derivative of value function wrt. wage, shaped [Nyprod,Nwages]
    """

    delta, r = params.delta, params.r

    Nwages, Nyprod = params.firm_wages.size, params.yprod.size

    # Value functions
    jupd = np.zeros((Nyprod, Nwages))
    jold = np.zeros((Nyprod, Nwages))

    # Derivatives
    djdw_upd = np.zeros((Nyprod, Nwages))
    djdw_old = np.zeros((Nyprod, Nwages))

    # Exogenous labor demand
    h = 1.0

    # Effective discount factor
    dfact = (1.0 - delta) / (1.0 + r)

    transm = params.yprod_transm

    for it in range(maxiter):
        for iy, y in enumerate(params.yprod):
            # Value function
            jupd[iy] = h*(y - params.firm_wages) + dfact*np.dot(transm[iy], jold)

            # Derivative
            djdw_upd[iy] = -h + dfact * np.dot(transm[iy], djdw_old)

        diff = np.amax(np.abs(jupd - jold))
        diff_djdw = np.amax(np.abs(djdw_upd - djdw_old))

        if diff < tol and diff_djdw < tol:
            break

        # Swap array references, continue to next iteration
        jupd, jold = jold, jupd
        djdw_upd, djdw_old = djdw_old, djdw_upd
    else:
        msg = 'Max. number of iterations exceeded'
        print(msg)

    J = jupd
    dJdw = djdw_upd

    return J, dJdw


def solve_search(params, J, dJdw):
    """
    Solve unmatched firms problem given the matched firms value function.

    Value of posting a vacancy:

        V*(y,w,theta) = - kappa + 1/(1+r) * q(theta) * E[J(y',w)|y]

    Whenever E[...] > 0, free entry implies that:

        kappa * (1+r) / q(theta) = E[J(y',w)|y]
            for all y, theta, w

        which pins down the equilibrium tightness theta for (y,w).

    On the other hand, whenever E[...] <= 0, for kappa > 0 no firm will
    choose to search in sub-market (y,w), and theta is thus not defined.

    For any operational submarket, we thus have
        q(theta) = kappa * (1+r) / E[J(y',w)|y]

    The derivative dtheta/dw can then be obtained from the implicit function
    rule if we write
            F(theta,w) = kappa * (1+r) / E[J(y',w)|y] - q(theta) = 0
    using
        dtheta/dw = - (dF/dw) / (dF/dtheta)
    We have
        dF/dw = - kappa * (1+r) / E[J(y',w)/dw|y]^2 * E[dJ(y',w)/dw|y]
        dF/dtheta = - (dq/dtheta)

    Parameters
    ----------
    params : defs.Params
    J : np.ndarray
        Value function of matched firm, shaped [Nyprod, Nwages]
    dJdw : np.ndarray
        Derivative of value function J wrt. w, shaped [Nyprod, Nwages]

    Returns
    -------
    thetas : np.narray
        Implied market tightness for each (yprod,wage), if value of
        vacancy is zero (otherwise undefined).
    dthetas_dw = np.ndarray
        Derivative of equilibrium market tightness wrt. wage for each
        (yprod,wage).
    pfun_search: np.ndarray
        Binary indicator whether firm will choose to post vacancy in market
        characterized by (yprod,wage).
    """

    kappa, r = params.kappa, params.r
    Nyprod, Nwages = params.yprod.size, params.firm_wages.size

    thetas = np.zeros((Nyprod, Nwages))
    dthetas_dw = np.zeros((Nyprod, Nwages))
    pfun_search = np.zeros((Nyprod, Nwages), dtype=np.bool_)

    # Precompute expectations E[J(y',w)|y] and its derivative wrt. w
    EJ = np.zeros((Nyprod, Nwages))
    dEJ_dw = np.zeros((Nyprod, Nwages))
    for iy in range(Nyprod):
        EJ[iy] = np.dot(params.yprod_transm[iy], J)
        dEJ_dw[iy] = np.dot(params.yprod_transm[iy], dJdw)

    for iy in range(Nyprod):
        for iw in range(Nwages):
            if EJ[iy, iw] <= 0.0:
                # No vacancy posting in this sub-market
                pfun_search[iy, iw] = False
                continue

            # For any E[...] > 0, compute implied equilibrium q:
            #   q(theta) = kappa * (1+r) / E[J(y',w)|y]
            q = kappa * (1.0 + r) / EJ[iy, iw]

            # Check that job-filling rate is valid. When matched firm value
            # is too low, job-filling rate > 1 would be needed to justify
            # posting vacancy (this is not an issue in continuous time since
            # there the rate is unbounded!)
            if q > 1.0:
                pfun_search[iy, iw] = False
                continue

            pfun_search[iy, iw] = True
            # Compute implied market tightness in this sub-market
            theta = prob_fill_inv(params, q)
            dtheta_dq = prob_fill_inv(params, q, deriv=True)
            # dtheta/dw = dtheta/dq * dq/dw
            # dq/dw = - kappa * (1+r) / E[J(y',w)|y]^2 * E[dJ(y',w)/dw|y]
            # Alternative method: compute derivative directly:
            dq_dw = - kappa * (1.0+r) / EJ[iy, iw]**2.0 * dEJ_dw[iy, iw]
            tmp = dtheta_dq * dq_dw

            # Derivative of theta wrt. w
            num = - kappa * (1.0 + r) / EJ[iy, iw]**2.0 * dEJ_dw[iy, iw]
            denom = - prob_fill(params, theta, deriv=True)
            dtheta_dw = - num / denom

            diff = dtheta_dw - tmp

            thetas[iy, iw] = theta
            dthetas_dw[iy, iw] = dtheta_dw

    return thetas, dthetas_dw, pfun_search
