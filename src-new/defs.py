"""
Author: Richard Foltyn
"""

import os

import numpy as np

from pydynopt.numba.helpers import create_numba_instance
from pydynopt.processes import tauchen, markov_ergodic_dist
from pydynopt.utils import powerspace


class Params:
    def __init__(self):

        # Interest rate
        self.r = 0.02

        # --- Firms ---

        # Exogenous separation probability
        self.delta = 0.05

        # Vacancy posting cost
        self.kappa = 0.03

        # Matching function elasticity
        self.alpha = 0.3

        # --- Households ---

        # Discount factor
        self.beta = 0.97

        # Risk aversion
        self.gamma = 1.0

        # --- Government ---

        # Unemployment benefits
        self.ui_ben = 0.4

        # Labor tax
        self.tau = 0.0

        # --- Idiosyncratic productivity ---

        # Number of discretized states
        self.yprod_N = 1
        # Autocorrelation of AR(1) process
        self.yprod_rho = 0.8
        # Conditional variance of AR(1) innovation
        self.yprod_var = 0.3**2.0

        # Discretized state space
        self.yprod = None
        # Transition matrix
        self.yprod_transm = None
        # Ergodic distribution
        self.yprod_edist = None

        # --- Grids ---

        # Wages for firm problem
        self.firm_wages_N = 10001
        self.firm_wages_min = self.ui_ben
        self.firm_wages_max = 10.0
        self.firm_wages = None

        # Cash on hand
        self.assets_N = 505
        self.assets_min = 0.0
        self.assets_max = 100.0
        self.assets = None

        # Wages for HH problem
        self.wages_N = 501
        self.wages_min = self.ui_ben
        self.wages_max = 1.4
        self.wages = None

        # Savings grid
        self.sav_N = 506
        self.sav_min = 0.0
        self.sav_max = self.assets_max + 10.0
        self.sav = None

    def create_derived(self):
        """
        Compute all grids, transition matrices, etc. from parameters stored
        in given object.
        """
        from math import sqrt

        # Discretized idiosyncratic productivity
        rho, sigma, N = self.yprod_rho, sqrt(self.yprod_var), self.yprod_N
        states, tm, edist, *rest = tauchen(rho, sigma, N, full_output=True)
        states = np.exp(states)
        self.yprod = states / np.dot(states, edist)
        self.yprod_transm = tm
        self.yprod_edist = edist

        # Wage grid for firm problem
        xmin, xmax, N = self.firm_wages_min, self.firm_wages_max, self.firm_wages_N
        self.firm_wages = np.linspace(xmin, xmax, N)

        # Cash-on-hand
        self.assets = powerspace(self.assets_min, self.assets_max, self.assets_N, 2.0)
        # Savings
        self.sav = powerspace(self.sav_min, self.sav_max, self.sav_N, 2.0)
        # Wage grid for HH problem
        xmin, xmax, N = self.wages_min, self.wages_max, self.wages_N
        self.wages = powerspace(xmin, xmax, N, 2.0)

    def to_numba(self):
        """
        Return instance of Params object that can be passed to numba-fied
        functions
        """

        # Create Numba-compatible instance
        params_nb = create_numba_instance(self)

        # Copy / reference attributes of numba-compatible objects
        copy_attributes(self, params_nb, copy=False)

        return params_nb


class Environment:
    def __init__(self):

        # If true, turn on diagnostics
        self.diag = False

        path = os.getcwd()
        scriptpath = os.path.dirname(os.path.abspath(__file__))
        if path == scriptpath:
            # Don't want result arrays, etc. do be created in current working
            # directory (which is scr-new/), move one folder up.
            basedir = os.path.abspath(os.path.join(path, '..'))
            self.graphdir = os.path.join(basedir, 'graphs')
            self.resultdir = os.path.join(basedir, 'results')
        else:
            self.graphdir = 'graphs'
            self.resultdir = 'results'

    def setup(self):
        """
        Set up environment, ie. create any output directories required to
        store graphs, etc.
        """

        import os

        dirs = (self.graphdir, self.resultdir)

        for d in dirs:
            os.makedirs(d, exist_ok=True)


class FirmSolution:

    def __init__(self):
        self.J = None
        self.dJ_dw = None
        self.thetas = None
        self.dthetas_dw = None
        self.pfun_search = None

    def to_numba(self):
        """
        Return instance of given object that can be passed to numba-fied
        functions.
        """

        firm_nb = create_numba_instance(self)
        copy_attributes(self, firm_nb, copy=False)

        return firm_nb


class HHSolution:

    def __init__(self):
        self.Ve = None
        self.Vu = None
        self.dVe_dw = None
        self.pfun_ce = None
        self.pfun_cu = None
        self.pfun_wage = None


def copy_attributes(src, dst, copy=True):
    """
    Copy attributes from src that are also present in dst into dst.

    Parameters
    ----------
    src : object
    dst : object
    copy : bool
        If true, copy array-valued attributes instead of referencing the
        original array.

    """

    for attr in dir(dst):
        if not attr.startswith('_') and hasattr(src, attr):
            x = getattr(src, attr)
            if x is not None:
                if (copy and not np.isscalar(x)) or isinstance(x, tuple):
                    x = np.copy(x)
                setattr(dst, attr, x)

    return dst
