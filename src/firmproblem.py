# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 17:24:30 2020

@author: jokr4584
"""

import numpy as np

#from ds_fun_noa import q_fun
#from ds_fun_noa import η_fun
from numba import jit


# @jit(nopython=True)
def firmproblem(η_fun,grids, sizes, wprod, r,x,κ,δ,ξ):
    """
    Compute value function of existing match, J(z,a,w).

    Searching firm
    --------------

    Value of a vacancy:

        V(z,theta,w) = max{0, - kappa + 1/(1+r) * q(theta) * E[J(z',a',w)|z]}

    Free entry implies that whenever E[...] > 0, we have:

        kappa * (1+r) / q(theta) = E[J(z',a',w)|z]
            for all z, theta, w


    Matched firm
    ------------

    Firm with ongoing match, exogenous separation probability delta
        (using V(z,theta,w) = 0):

        J(z,a,w) = max_h { h(z-w) - 0.5*xi*(1-h)^2 + (1-delta)/(1+r) * E[J(z',a',w)|z]}

    Currenty, we omit the dependence on a, and skip the maximization problem,
    so we have:

        J(z,w) = h(z-w) + (1-delta)/(1+r) * E[J(z',w)|z]

    Parameters
    ----------
    η_fun
    grids
    sizes
    wprod
    r
    x
    κ
    δ
    ξ

    Returns
    -------

    """

    size_y, size_w, size_a,_,_ = sizes
    grid_y, grid_w, grid_a,trans,_,_ = grids
    
    #Endogenous quits
    # δ_w = np.zeros((size_y,size_A,size_w) ,dtype= np.float64)
    Jvec=  np.ones((size_y,size_w) ,dtype= np.float64)
    Jnew=  np.zeros((size_y,size_w) ,dtype= np.float64)
    Jprime=  np.zeros((size_y,size_w) ,dtype= np.float64)
    dJnew=  np.zeros((size_y,size_w) ,dtype= np.float64)
    dJnew_new=  np.zeros((size_y,size_w) ,dtype= np.float64)
    dJsearch=  np.zeros((size_y,size_w) ,dtype= np.float64)
    
    Jsearch = Jvec*(Jvec>κ*(1+r)) + κ*(1+r)*(Jvec<=κ*(1+r))
    grid_qw =  κ*(1+r)/Jsearch
    grid_θw = (((grid_qw**(-x))-1))**(1/x)                                                  
    prob_eta = η_fun(grid_θw,x)    
    
    diffJ = 10
    while diffJ >10**-10:     
        Jsearch = trans@Jprime*(trans@Jprime>κ*(1+r)) + κ*(1+r)*(trans@Jprime<=κ*(1+r)) ##should average first, then ditch zeros, not other way around

        for iy in range(size_y):
            for iw in range(size_w):
                Jnew[iy,iw] = (wprod*grid_y[iy]-grid_w[iw])+ (1-δ)/(1+r) * trans[iy]@Jprime[:,iw]
                grid_qw[iy,iw] =  κ*(1+r)/(Jsearch[iy,iw])
            
            #Derivative of Jnew
            dJnew[iy] =-1 + (1-δ)/(1+r) * trans[iy]@dJnew_new
# 
        diffJ = np.max(np.abs(Jnew-Jprime))
        Jnew,Jprime = Jprime,Jnew

        dJnew,dJnew_new=dJnew_new,dJnew

        out = np.empty_like(grid_qw)
        grid_θw = (np.round_(grid_qw,14,out)**(-x)-1)**(1/x)                                                  
        prob_eta= η_fun(grid_θw,x) 
        
        dJsearch =trans@dJnew*(trans@Jprime>κ*(1+r))
#    dηdθ = 1/((1+grid_θw**x)**(1/x)) - grid_θw**x/((1+grid_θw**x)**(1/x+1))
#    dqdθ = dqdθ_fun(grid_θw,x)    
        
    return prob_eta, grid_θw, grid_qw, dJsearch