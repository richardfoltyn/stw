# -*- coding: utf-8 -*-
"""
Created on Sun Mar 29 09:48:23 2020

@author: jokr4584
"""

import numpy as np
from numba import jit

############ UTILITY FUNCTIONS
#Vectors
@jit(nopython=True)
def u_vec(c,γ ):
    u = np.zeros(c.shape, dtype= np.float64)
    t = c>0 #check that consumption is positive
    u[t] = (c[t]**(1-γ)-1)/(1-γ)
    u[np.invert(t)]=-10000
    return u

#Scalars
@jit(nopython=True)
def u_scalar(c,γ ):
    if c>0: #check that consumption is positive
        u = (c**(1-γ)-1)/(1-γ)
    else:
        u=-10000
    return u

@jit(nopython=True)
def u_prime_v(c,γ ):
    up = np.zeros(c.shape, dtype= np.float64)
    t = c>0 #check that consumption is positive
    up[t] = c[t]**(-γ)
    up[np.invert(t)]=10000
    return up

@jit(nopython=True)
def u_prime_s(c,γ ):
    if c>0: #check that consumption is positive
        up = c**(-γ)
    else:
        up=10000
    return up

@jit(nopython=True)
def u_primeinv_v(m,γ ):
    upinv = np.zeros(m.shape, dtype= np.float64)
    t = m>0 #check that consumption is positive
    upinv[t] = m[t]**(-1/γ)
    upinv[np.invert(t)]=10000
    return upinv

@jit(nopython=True)
def u_primeinv_s(m,γ ):
    if m>0: #check that consumption is positive
        upinv = m**(-1/γ)
    else:
        upinv=10000
    return upinv

