# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 17:57:23 2020

@author: jokr4584
"""

import numpy as np
import linear  #From Richard
#from ds_fun_noa import basefun 
import utility as ut
from numba import jit

@jit(nopython=True)
def egm_ds(basefun, grid_A, grids, sizes, B, β, r, Σ, τ, γ,δ, prob_eta, opt_w, lowbound,grid_xe, g_ce, grid_xu, g_cu):

    size_y, size_w, _,_,_ = sizes
    grid_y, grid_w, _,trans,_,_ = grids    
    
    size_A = grid_A.size
    
    cprime_e = np.zeros((size_y,size_w),dtype= np.float64)
    cprime_u = np.zeros((size_y),dtype= np.float64)
    
    #new coh grid after update
    grid_xu_new = np.ones((size_y,size_A), dtype= np.float64)
    grid_xe_new = np.ones((size_y,size_A,size_w), dtype= np.float64)
    
    g_cu_new = np.ones((size_y,size_A), dtype= np.float64)
    g_ce_new = np.ones((size_y,size_A,size_w), dtype= np.float64)


    for ia in range(1,size_A):
        xprime_u = (1-τ)*B + (1+r)*grid_A[ia] 
        #Calculating next period's consumptions
        for iyp in range(size_y):
            xprime_e = (1-τ)*grid_w + (1+r)*grid_A[ia] 

            #Resulting consumption next period
            cpu = linear.interp1d_scalar(xprime_u, np.hstack((np.array([0]), grid_xu[iyp])), np.hstack((np.array([0]), g_cu[iyp])))
            cprime_u[iyp]= cpu * (cpu<= xprime_u ) + xprime_u*(cpu>xprime_u )
            
            for iwp in range(size_w):
                #Resulting consumption
                cpe = linear.interp1d_scalar(xprime_e[iwp], np.hstack((np.array([0]), grid_xe[iyp, :, iwp])), np.hstack((np.array([0]), g_ce[iyp, :, iwp])))
                cprime_e[iyp,iwp] = cpe * (cpe<=xprime_e[iwp]) + xprime_e[iwp]*(cpe>xprime_e[iwp])      
                
        #Todays consumption, given tomorrows
        for iy in range(size_y):
            transy = trans[iy]
            w1,w2 = basefun(grid_w,size_w,opt_w[iy,ia])
            cpe_int = w1[0]*ut.u_prime_v(cprime_e[:,w2[0]],γ) +w1[1]*ut.u_prime_v(cprime_e[:,w2[1]],γ)
            pe_int =  w1[0]*prob_eta[iy,w2[0]]+w1[1]*prob_eta[iy,w2[1]]
  
            #New consumption rules -- Euler Equation
            g_cu_new[iy,ia] =  ut.u_primeinv_s(β*(1+r)*((1-pe_int)*transy@ut.u_prime_v(cprime_u,γ) + \
                                                           pe_int *transy@cpe_int),γ)       

            for iw in range(size_w):
                g_ce_new[iy,ia,iw] = ut.u_primeinv_s(β*(1+r)*(  (1-δ)*transy@ut.u_prime_v(cprime_e[:,iw],γ) + \
                                                                   δ *transy@ut.u_prime_v(cprime_u,γ)),γ)

        #Recover new endogenous grid -- coh last period
        grid_xe_new[:,ia,:]  = grid_A[ia] + g_ce_new[:,ia,:]   
        grid_xu_new[:,ia]    = grid_A[ia] + g_cu_new[:,ia]
   
    
    return  grid_xe_new, grid_xu_new, g_ce_new, g_cu_new






        