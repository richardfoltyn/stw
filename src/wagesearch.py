# -*- coding: utf-8 -*-
"""
Created on Tue Jul 21 17:21:56 2020

@author: jokr4584
"""
import numpy as np
from numba import jit


@jit(nopython=True)
def dqdθ_fun(θ,x):
    dqdθ=- np.ones(θ.shape)
    test = θ>0
    N = -(θ + np.invert(test))**(x-1)
    D = ((1+θ**x)**(1/x+1))
    dqdθ=N/D-1000000*np.invert(test)
    return dqdθ

@jit(nopython=True)
def ws_u_analytical(grids, sizes, x,r,κ,Σ,τ,ξ,dJsearch,grid_qw,grid_θw, prob_eta, V_ep, V_up, d_Ve_prime, g_ue_prime,dxdw):

    size_y, size_w, size_a,_,_ = sizes
    grid_y, grid_w, grid_a,trans,_,_ = grids
    
    opt_w = np.zeros((size_y,size_a), dtype = np.float64)
   
    #Find the optimal market to search in, given the opportunity
    dηdθ = 1/((1+grid_θw**x)**(1/x)) - grid_θw**x/((1+grid_θw**x)**(1/x+1))
    dqdθ = dqdθ_fun(grid_θw,x)
 
    for iy in range(size_y):
        transy = trans[iy]
        dθdw = -grid_qw[iy]*(dJsearch[iy])/(dqdθ[iy]*(κ*(1+r)/grid_qw[iy]))
        
       
        for ia in range(size_a):
                    
             Vdiff =(trans[iy]@V_ep[:,ia]-trans[iy]@V_up[:,ia])
            
            
             d_cont =- (dηdθ[iy]*dθdw*Vdiff+prob_eta[iy]*( \
                        transy@( dxdw*g_ue_prime[:,ia] +\
                        d_Ve_prime[:,ia])))
             ss =1
             iw = -1
             while (ss >0) & (iw <size_w-3):
                 iw +=1
                
                 if np.sign(d_cont[iw])!= np.sign(d_cont[iw+1]):
                     slope =  (d_cont[iw+1]-d_cont[iw])/(grid_w[iw+1]-grid_w[iw])
                     dw = -d_cont[iw]/slope
                     opt_w[iy,ia] = grid_w[iw]+dw
                     ss = -1
                 if iw == size_w-3:
                     opt_w[iy,ia] = grid_w[-1]   
                     
    return opt_w
               
               
@jit(nopython=True)
def ws_u_numdiff(grids, sizes, prob_eta, V_ep, V_up ):
    size_y, size_w, size_a,_,_ = sizes
    grid_y, grid_w, grid_a,trans,_,_ = grids
    opt_w2 = np.zeros((size_y,size_a), dtype = np.float64)
    
    for iy in range(size_y):
        transy = trans[iy]
        
        for ia in range(size_a):    
        
            contval = (1-prob_eta[iy])*(transy@V_up[:,ia]) + prob_eta[iy]*(transy@V_ep[:,ia,:])
            temp1 = np.hstack((np.array([0]),np.diff(contval)/np.diff(grid_w)))
            temp2 = np.hstack((np.diff(contval)/np.diff(grid_w),np.array([0])))
            d_cont2 = -temp1/2-temp2/2
            
#            Vdiff =(trans[iy]@V_ep[:,ia]-trans[iy]@V_up[:,ia])
#            d_cont3 = -np.diff(prob_eta[iy])/np.diff(grid_w)*Vdiff[0:-1]-prob_eta[iy,0:-1]*(transy@(np.diff(V_ep[:,ia,:])/np.diff(grid_w)))
#            
#            d_cont4 = -np.diff(prob_eta[iy])/np.diff(grid_w)*Vdiff[0:-1]-prob_eta[iy,0:-1]* \
#                        (transy@((1-τ)*g_ue_prime[:,ia,0:-1]+\
#                        d_Ve_prime[:,ia,1:]))
#            d_cont5 = -dηdθ[iy,0:-1]*dθdw[0:-1]*Vdiff[0:-1]-prob_eta[iy,0:-1]*(transy@((1-τ)* \
#                           g_ue_prime[:,ia,0:-1] +\
#                           d_Ve_prime[:,ia,0:-1]))
#            d_cont6 = -dηdθ[iy,0:-1]*dθdw[0:-1]*Vdiff[0:-1]-prob_eta[iy,0:-1]*(transy@(np.diff(V_ep[:,ia,:])/np.diff(grid_w)))
              
            ss =1
            iw2 = -1
            while (ss >0) & (iw2 <size_w-3):
                iw2 +=1
                
                if np.sign(d_cont2[iw2])!= np.sign(d_cont2[iw2+1]):
                    slope =  (d_cont2[iw2+1]-d_cont2[iw2])/(grid_w[iw2+1]-grid_w[iw2])
                    dw = -d_cont2[iw2]/slope
                    opt_w2[iy,ia] = grid_w[iw2]+dw
                    ss = -1
                if iw2 == size_w-3:
                    opt_w2[iy,ia] = grid_w[-1]

    return opt_w2
                
                
                
                
                
                
                
                
                
                
                