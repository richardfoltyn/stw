# -*- coding: utf-8 -*-
"""
Created on Fri Feb 28 15:23:24 2020

@author: jokr4584
"""
import numpy as np

from numba import jit
#User written functions
import linear  #From Richard
import utility as ut
import scipy
from wagesearch import ws_u_analytical
from wagesearch import ws_u_numdiff
from firmproblem import firmproblem
from egm_ds import egm_ds


@jit(nopython=True)
def q_fun(θ,x):
    q = 1/((1+θ**x)**(1/x))
    
    return q

###############################################################################
#Matching function
@jit(nopython=True)
def η_fun(θ,x):
    η = θ/((1+θ**x)**(1/x))
    
    return η

@jit(nopython=True)
def dqdθ_fun(θ,x):
    dqdθ=- np.ones(θ.shape)
    test = θ>0
    N = -(θ + np.invert(test))**(x-1)
    D = ((1+θ**x)**(1/x+1))
    dqdθ=N/D-1000000*np.invert(test)
    return dqdθ

#Stationary markov distribution
@jit(nopython=True)
def solveStationary( A ):
    S, U = np.linalg.eig(A.T)
    stationary = np.array(U[:, np.where(np.abs(S - 1.) < 1e-8)[0][0]].flat, dtype= np.float64)
    stationary = stationary / np.sum(stationary)
    return stationary

#Searching for vlaue along grid -- Bisection search
@jit(nopython= True)
def basefun(grid_x,npx,x):

    jl=0;      
    ju=npx-1;	

    #This finds where the x value is along grid_x
    while ju-jl>1 :   
        jm=round((ju+jl)/2)
        if x>=grid_x[jm]:
            jl=jm
        else:
            ju=jm
    
    vals = np.array([0.0,0.0], dtype = np.float64)
    inds = np.array([0,0], dtype = np.int64)
    
    #Violante way of calculating where to allot distribution
    i=jl+1

    vals[1]=(x-grid_x[i-1])/(grid_x[i]-grid_x[i-1])
    vals[1]=max(0,min(1,vals[1]))
    vals[0]=1-vals[1]
    
    inds[1]=i
    inds[0]=i-1
    return vals, inds
    

@jit(nopython=True)
def own_tile_xrep(vec, num):
    temp = np.zeros((vec.size,num), dtype= np.float64)
    for i in range(num):
        temp[:,i] = vec
        
    return temp

@jit(nopython=True)
def own_tile_yrep(vec, num):
    temp = np.zeros((num,vec.size), dtype= np.float64)
    for i in range(num):
        temp[i,:] = vec
        
    return temp

        
        
###############################################################################
############## Consumer's Value Function -- Version using A instead of X
# @jit(nopython=True)
def cons_egmA(τ,sizes, grids, greeks):
    
    size_y, size_w, size_a,_,_ = sizes
    grid_y, grid_w, grid_a,trans,_,_ = grids
    β,r,γ,δ,_,κ,ξ,Σ,x,B,_,wprod,Λ = greeks

#    γ=0.00001
#    r = 1/β-1-0.000001
    grid_A = grid_a #*0.000001
    size_A = grid_A.size

    #Value function
    V_e = np.ones((size_y,size_A,size_w),dtype= np.float64)
    V_u = np.ones((size_y,size_A), dtype= np.float64)
    
    #Updated value function
    V_enew = np.ones((size_y,size_A,size_w),dtype= np.float64)
    V_unew = np.ones((size_y,size_A), dtype= np.float64)
    
    #Value 
    V_up = np.zeros((size_y,size_A),dtype= np.float64)
    V_ep = np.zeros((size_y,size_A,size_w) ,dtype= np.float64)
    
    d_Ve= np.zeros((size_y,size_A,size_w),dtype = np.float64)
    d_Ve_new= np.zeros((size_y,size_A,size_w),dtype = np.float64)
    d_Ve_prime= np.zeros((size_y,size_A,size_w),dtype = np.float64) 

    g_ue_prime= np.zeros((size_y,size_A,size_w),dtype = np.float64)  

    extrap_cu= np.zeros((size_y),dtype = np.float64)  
    extrap_ce= np.zeros((size_y,size_w),dtype = np.float64)  

    mat_w = np.zeros((size_y,size_w))
    for iy in range(size_y):
        mat_w[iy] = grid_w
    dxdw = (1-τ)
        
    ## Initialize for EGM
    grid_xu = np.zeros((size_y,size_A), dtype=np.float64)
    grid_xe = np.zeros((size_y,size_A,size_w),dtype= np.float64)
    for iiy in range(size_y):
        grid_xu[iiy,:] = grid_A
        for iiw in range(size_w):
            grid_xe[iiy,:,iiw] = grid_A
    
    #Initial consumption rule guess
    g_cu = np.copy(grid_xu)/2
    g_ce = np.copy(grid_xe)/2  
    
    ## Solve the firm's problem
    prob_eta, grid_θw, grid_qw, dJsearch = firmproblem(η_fun,grids, sizes, wprod,r,x,κ,δ,ξ)


    diff = 10
    c=1
    lowbound = (1-τ)*B /2           
    while (diff > 10**(-8)) & ((c<600)):
        
        ## Optimal Wage Choice       
        opt_w2 = ws_u_analytical(grids, sizes, x,r,κ,Σ,τ,ξ,dJsearch,grid_qw,grid_θw, prob_eta, V_ep, V_up, d_Ve_prime, g_ue_prime,dxdw)
        opt_w = ws_u_numdiff(grids, sizes, prob_eta, V_ep, V_up)

        ## Update Policy functions
        grid_xe_new, grid_xu_new, g_ce_new, g_cu_new = egm_ds(basefun, grid_A, grids, sizes, B, β, r, Σ, τ, γ,δ, prob_eta, opt_w, lowbound, \
                                                              grid_xe, g_ce, grid_xu, g_cu)

        grid_xe_new, grid_xe = grid_xe, grid_xe_new
        grid_xu_new, grid_xu = grid_xu, grid_xu_new
        
        g_cu_new,g_cu = g_cu, g_cu_new
        g_ce_new,g_ce = g_ce, g_ce_new       
    
        #Need to append the grids to include points on the x-grid that are possible
        #to reach but that entail a'=0 and also to the right
        grid_xu[:,0] = np.ones(size_y)*lowbound
        g_cu[:,0]    = np.ones(size_y)*lowbound
        
        for iw in range(size_w):
            grid_xe[:,0,iw] = np.ones(size_y)*lowbound
            g_ce[:,0,iw]    = np.ones(size_y)*lowbound         #This becomes a problem if income is too low --> c=0!
        
        #Linearly extrapolate consumption to *very* high COH values
        for iyp in range(size_y):
            for iwp in range(size_w):
                extrap_cu[iyp] = linear.interp1d_scalar(10000, grid_xu[iyp], g_cu[iyp])
                extrap_ce[iyp,iwp] = linear.interp1d_scalar(10000, grid_xe[iyp, :, iwp], g_ce[iyp, :, iwp])
        
        
        c+=1
#        diff = np.max(np.abs(g_cu-g_cu_new))+np.max(np.abs(g_ce-g_ce_new))
        if c%100==0: ##Only print every 100th time
            print(diff)
#            plt.plot(np.transpose(grid_xu),np.transpose(opt_w))
#            plt.plot(np.transpose(grid_xu),np.transpose(opt_w2))
#            plt.pause(0.01)
                 
        cv=0

        while cv < 3 :
            #Replace 1st entry with coh. Otherwise not filled to left of a'=0
            
            cp =np.hstack(( np.array([(1-τ)*B]), (1-τ)*B + (1+r)*grid_A[1:])) #COH tomorrow, given a' today

            for iyp in range(size_y):
#                Vu_interp = interp1d(grid_xu[iyp], V_u[iyp], kind='cubic',fill_value="extrapolate")
#                V_up[iyp,:] = Vu_interp(cp)
                V_up[iyp,:] = linear.interp1d_array(cp, grid_xu[iyp], V_u[iyp])
                
                for iwp in range(size_w): #Wage tomorrow
                    #COH tomorrow given COH today
                    lbe =  (1-τ)*grid_w[iwp]
                    ep = np.hstack((np.array([lbe]),(1-τ)*grid_w[iwp] + (1+r)*grid_A[1:]))
                    
                    #Value function given tomorrow's y, tomorrow's w, and tomorrow's INITIAL assets, i.e.
                    #today's savings choice a' 
                    V_ep[iyp,:,iwp] =       linear.interp1d_array(ep, grid_xe[iyp, :, iwp], V_e[iyp, :, iwp])
                    d_Ve_prime[iyp,:,iwp] = linear.interp1d_array(ep, grid_xe[iyp, :, iwp], d_Ve[iyp, :, iwp])

#                    Ve_interp = interp1d(grid_xe[iyp,:,iwp], V_e[iyp,:,iwp], kind='cubic',fill_value="extrapolate") 
#                    V_ep[iyp,:,iwp] =  Ve_interp(ep)
#
#                    d_Ve_interp = interp1d(np.hstack((grid_xe[iyp,:,iwp],np.array([10000]))), \
#                                           np.hstack((d_Ve[iyp,:,iwp],np.array([0]))), kind='cubic',fill_value="extrapolate") 
#                    d_Ve_prime[iyp,:,iwp] = d_Ve_interp(ep)

                    g_ue_prime[iyp,:,iwp] = ut.u_prime_v(
                        linear.interp1d_array(ep, grid_xe[iyp, :, iwp], \
                                         g_ce[iyp,:,iwp]), γ)
            
            #Value function tomorrow (prime) if a' was saved yesterday
            for iy in range(size_y):
                for ix in range(size_A):
                    w1,w2 = basefun(grid_w,size_w,opt_w[iy,ix]) ##Which wage do unemployed choose on the grid?
                    V_ep_int = w1[0]*V_ep[:,ix,w2[0]]  +w1[1]*V_ep[:,ix,w2[1]]
                    pe_int =   w1[0]*prob_eta[iy,w2[0]]+w1[1]*prob_eta[iy,w2[1]]
                    
                    transy = trans[iy]
                    V_unew[iy,ix] = ut.u_scalar(g_cu[iy,ix],γ) + β*((1-pe_int)*transy@V_up[:,ix] \
                                                                     + pe_int* transy@V_ep_int)

                    for iw in range(size_w):
                        V_enew[iy,ix, iw] = ut.u_scalar(g_ce[iy,ix,iw],γ) +  β*(δ*transy@V_up[:,ix] + \
                                                                            (1-δ)*transy@V_ep[:,ix,iw] )
              
                        d_Ve_new[iy,ix,iw] =  β*(1-δ)*(transy@d_Ve_prime[:,ix,iw]+ \
                               transy@(dxdw*g_ue_prime[:,ix,iw]))
                        
            cv+=1 
            V_e,V_enew = V_enew,V_e
            V_u,V_unew = V_unew,V_u
            d_Ve, d_Ve_new = d_Ve_new, d_Ve

        diff= np.max(np.abs(V_u-V_unew)) +np.max(np.abs(V_e-V_enew))
#        print('VFI: ' , np.sum(np.abs(V_u-V_unew)) +np.sum(np.abs(V_e-V_enew))  )           
        V_unew = np.copy(V_u)
        V_enew = np.copy(V_e)
    
    print('---------- Value function Solved ------------')
    return grid_xe, grid_xu, g_ce, g_cu, opt_w,V_up,V_ep,V_u,V_e, d_Ve, prob_eta,grid_θw







###############################################################################
###############################################################################
###############################################################################
######## Filling the distribution (Steady State)    
#@jit(nopython= True)
def distfill(τ,greeks, sizes, grids, policies):
    
    size_y, _, _,size_afine,size_wfine = sizes
    _,_, _,_,trans,grid_afine,grid_wfine,grid_hfine = grids 
    grid_xufine, grid_xefine, opt_wfine, g_pufine = policies
    
    totnum = size_y*(size_afine+size_afine*size_wfine) #Unemployed only COH, employed COH and w
    transmat = scipy.sparse.lil_matrix((totnum,totnum))

                
    β,r,γ,δ,_,κ,ξ,Σ,x,B,_,wprod,Λ = greeks
    
    for ix in range(size_afine):   #Asset state
        #Enter the period with coh = ix -- find coh tomorrow
        ap = grid_afine[ix]   #Savings decision -- doesn't depend on y
#        wp = opt_wfine[:,ix]  #Wage Decision
#        ηp = g_pufine[:,ix]   #Probability associated with Wage

        ### Tomorrow's COH if unemployed tomorrow
        coh_u = (1+r)*ap+(1-τ)*B #doesn't depend on y
        for iy in range(size_y):

            #Tomorrow's wage
            w1,w2 = basefun(grid_wfine,size_wfine,opt_wfine[iy,ix])
            ηy = g_pufine[iy,ix]
 
            for iyp in range(size_y):
                ### Tomorrow's COH if one becomes unemployed
                vals_u, inds_u = basefun(grid_xufine[iyp],size_afine,coh_u)

                ### Tomorrow's COH if one finds employment
                coh_ue0 = (1+r)*ap+(1-τ)*grid_wfine[w2[0]]*(grid_hfine[iyp,w2[0]]+Σ*(1-grid_hfine[iyp,w2[0]]))
                vals_ue0, inds_ue0 = basefun(grid_xefine[iyp,:,w2[0]],size_afine,coh_ue0)
    
                coh_ue1 = (1+r)*ap+(1-τ)*grid_wfine[w2[1]]*(grid_hfine[iyp,w2[1]]+Σ*(1-grid_hfine[iyp,w2[1]]))
                vals_ue1, inds_ue1 = basefun(grid_xefine[iyp,:,w2[1]],size_afine,coh_ue1)


                tu = trans[iy,iyp]
                cell_ufrom = ix*size_y+iy
                ### U2U transitions
                transmat[cell_ufrom,inds_u[0]*size_y+iyp] += tu*vals_u[0]*(1-ηy) 
                transmat[cell_ufrom,inds_u[1]*size_y+iyp] += tu*vals_u[1]*(1-ηy) 
    
                ### U2E transitions 
                transmat[cell_ufrom,size_y*(size_afine+ size_wfine*(inds_ue0[0]) + w2[0])+iyp] +=  tu*vals_ue0[0]*ηy*w1[0]
                transmat[cell_ufrom,size_y*(size_afine+ size_wfine*(inds_ue0[1]) + w2[0])+iyp] +=  tu*vals_ue0[1]*ηy*w1[0]
                
                transmat[cell_ufrom,size_y*(size_afine+ size_wfine*(inds_ue1[0]) + w2[1])+iyp] +=  tu*vals_ue1[0]*ηy*w1[1]
                transmat[cell_ufrom,size_y*(size_afine+ size_wfine*(inds_ue1[1]) + w2[1])+iyp] +=  tu*vals_ue1[1]*ηy*w1[1]

                for iw in range(size_wfine):   #Wage state
        
                    #Tomorrow's COH if one stays employed
                    coh_ee = (1+r)*ap+(1-τ)*grid_wfine[iw]*(grid_hfine[iyp,iw]+Σ*(1-grid_hfine[iyp,iw])) 
                    valse, indse = basefun(grid_xefine[iyp,:,iw],size_afine,coh_ee)
                    
                    te =  trans[iy,iyp]
        
                    cell_efrom = size_y*(size_afine+size_wfine*ix+iw)+iy
                    
                    #Stay in same job
                    transmat[cell_efrom, size_y*(size_afine+size_wfine*indse[0]+iw)+iyp] += te*valse[0]*(1-δ)
                    transmat[cell_efrom, size_y*(size_afine+size_wfine*indse[1]+iw)+iyp] += te*valse[1]*(1-δ)
        
                    ### E2U transitions
                    transmat[cell_efrom, size_y*inds_u[0]+iyp] += te*vals_u[0]*δ
                    transmat[cell_efrom, size_y*inds_u[1]+iyp] += te*vals_u[1]*δ 

    B = scipy.sparse.csr_matrix(transmat)
    _,evec = scipy.sparse.linalg.eigs(np.transpose(B),k=1,sigma=1)
    stationary = evec/np.sum(evec)
    stationary = np.real(stationary)
    
    
    dist_e = np.zeros((size_y,size_afine,size_wfine), dtype = np.float64)
    dist_u = np.zeros((size_y,size_afine), dtype = np.float64)
 
    for ix in range(size_afine):
        for iy in range(size_y):
            dist_u[iy,ix] = stationary[ix*size_y+iy]
            for iw in range(size_wfine):
                dist_e[iy,ix,iw]= stationary[size_y*(size_afine + size_wfine*ix+iw)+iy]
        
    return dist_e, dist_u


################################################################################
@jit(nopython= True)
def finegrids(grids, sizes,grid_xe, grid_xu, g_ce, g_cu, opt_w,prob_eta,grid_θw,lowbound):
    size_y, size_w, size_a,size_afine,size_wfine = sizes
    grid_y, grid_w, grid_a,_,trans,grid_afine,grid_wfine,grid_hfine = grids 


    g_pu = np.zeros((size_y,size_a), dtype=np.float64)
    
    for iy in range(size_y):
        for ia in range(size_a):
            w1,w2 = basefun(grid_w,size_w,opt_w[iy,ia]) ##Which wage do unemployed choose on the grid?
            g_pu[iy,ia] = w1[0]*prob_eta[iy,w2[0]]+w1[1]*prob_eta[iy,w2[1]]
            
    
    ####### Interpolate the policy functions on a finer grid
    g_cefine = np.zeros((size_y,size_afine, size_wfine) , dtype = np.float64)
    g_cufine = np.zeros((size_y,size_afine) , dtype = np.float64)

    grid_xefine = np.zeros((size_y,size_afine, size_wfine) , dtype = np.float64)
    grid_xufine = np.zeros((size_y,size_afine) , dtype = np.float64)
    
    g_wufine = np.zeros((size_y,size_afine), dtype=np.float64)
        
    g_pufine = np.zeros((size_y,size_afine), dtype=np.float64)
    
    grid_θwfine = np.zeros((size_y,size_afine) , dtype = np.float64)
    
    for iy in range(size_y):
        #Interpolating decision rules onto finer grid
        grid_xufine[iy]   = np.interp(grid_afine,grid_a,grid_xu[iy])
        g_cufine[iy]      = grid_xufine[iy,:]-grid_afine
        g_pufine[iy]      = np.interp(grid_afine,grid_a,g_pu[iy]) ## Shouldn't be interpolated like this!
        g_wufine[iy]      = np.interp(grid_afine,grid_a,opt_w[iy]) ## Shouldn't be interpolated like this!

        grid_θwfine[iy] =  np.interp(grid_wfine, grid_w, grid_θw[iy])
        
        for ia in range(size_afine):  
            for iw in range(size_wfine):
                wval ,wind=  basefun(grid_w,size_w,grid_wfine[iw])
                
                #Consumption choice for the employed
                c0 = linear.interp1d_scalar(grid_afine[ia], grid_a, g_ce[iy, :, wind[0]])
                c1 = linear.interp1d_scalar(grid_afine[ia], grid_a, g_ce[iy, :, wind[1]])
                g_cefine[iy,ia,iw] = wval[0]*c0 + wval[1]*c1
                grid_xefine[iy,ia,iw] = g_cefine[iy,ia,iw] + grid_afine[ia]
                
        #Need to append the grids to include points on the x-grid that are possible
        #to reach but that entail a'=0
        grid_xufine[:,0] = np.ones((size_y))*lowbound
        g_cufine[:,0] = np.ones(size_y)*lowbound
        
        for iw in range(size_wfine):
            grid_xefine[:,0,iw] = np.ones((size_y))*lowbound # ((1-τ)*grid_w[iw]*(grid_h[:,iw] +  0.6*(1-grid_h[:,iw])) + fp)
            g_cefine[:,0,iw] = np.ones(size_y)*lowbound #  ((1-τ)*grid_w[iw]*(grid_h[:,iw] +  0.6*(1-grid_h[:,iw])) + fp)

    return grid_xefine,grid_xufine,g_cefine,g_cufine,g_pufine,g_wufine, grid_θwfine         


'''
###################
###################
Transition code
###################
###################
'''

@jit(nopython= True)
def egm_trans(TT,greeks, sizes, grids, transgrids, EGMres, τ, dJT, r_T ):

    β,r,γ,δ,_,κ,_,Σ,x,B,_,_,Λ = greeks
    size_y, size_w, size_a,size_afine,size_wfine = sizes
    _,grid_w, grid_a,_,trans,grid_afine,grid_wfine,grid_hfine = grids 
    
    prob_etaT, grid_hT,grid_qwT, grid_θwT = transgrids
    grid_xe, grid_xu, g_ce, g_cu, opt_w, V_up, V_ep, V_u, V_e,d_Ve = EGMres 

    grid_A = grid_a
    size_A = grid_A.size
    
    #Initialize grids
    grid_xeT = np.zeros((TT,size_y,size_a,size_w),dtype=np.float64) #Savings grid
    grid_xeT[TT-1] = EGMres[0]
    grid_xuT = np.zeros((TT,size_y,size_a),dtype=np.float64)
    grid_xuT[TT-1] = EGMres[1]
    g_ceT = np.zeros((TT,size_y,size_a,size_w),dtype=np.float64) #Consumption policy
    g_ceT[TT-1] = EGMres[2]
    g_cuT = np.zeros((TT,size_y,size_a),dtype=np.float64)
    g_cuT[TT-1] = EGMres[3]
    opt_wT =  np.zeros((TT,size_y,size_a),dtype=np.float64) #Optimal wage choice
    opt_wT[TT-1] = EGMres[4]
    
    cprime_e = np.zeros((size_y,size_w),dtype= np.float64)
    cprime_u = np.zeros((size_y),dtype= np.float64)
    
    
    opt_w =  np.zeros((size_y,size_A),dtype=np.float64) #Optimal wage choice
    V_upT = np.zeros((TT,size_y,size_A),dtype=np.float64) #Value function tomorrow, given x
#    V_upT[TT-1] = EGMres[5]
    V_epT = np.zeros((TT,size_y,size_A,size_w),dtype=np.float64)
    d_Ve_prime = np.zeros((TT,size_y,size_A,size_w),dtype=np.float64)
#    V_epT[TT-1] = V_ep
    V_uT = np.zeros((TT,size_y,size_A),dtype=np.float64) #Value function today, given x
    V_uT[TT-1] = EGMres[7]
    V_eT = np.zeros((TT,size_y,size_A,size_w),dtype=np.float64) #Value function employed today
    V_eT[TT-1] = EGMres[8]  
    dVe_T= np.zeros((TT,size_y,size_A,size_w),dtype=np.float64) #Value function employed today
    dVe_T[TT-1] = EGMres[9]  
    
    lowbound = (1-τ)*B/2
    
    grid_xu = np.zeros((size_y,size_A), dtype=np.float64)
    grid_xunew = np.zeros((size_y,size_A), dtype=np.float64)
    grid_xu = grid_xuT[TT-1]
    
    g_cu = np.zeros((size_y,size_A), dtype=np.float64)
    g_cunew = np.zeros((size_y,size_A), dtype=np.float64)
    g_cu = g_cuT[TT-1]
    
    grid_xe = np.zeros((size_y,size_A,size_w),dtype= np.float64)
    g_ce =np.zeros((size_y,size_A,size_w),dtype= np.float64)
    grid_xenew = np.zeros((size_y,size_A,size_w),dtype= np.float64)
    g_cenew =np.zeros((size_y,size_A,size_w),dtype= np.float64)
        
    grid_xe = np.copy(grid_xeT[TT-1])
    g_ce = np.copy(g_ceT[TT-1])    
    
    for t in range(TT-2,-1,-1):    
        
        #Calculate V' given  assets 
        cp = np.hstack(( np.array([(1-τ)*B]), (1-τ)*B + (1+r_T[t+1])*grid_A[1:] ))
        for iyp in range(size_y):
            V_upT[t,iyp,:] = np.interp(cp ,grid_xu[iyp] ,V_uT[t+1,iyp])
            
            for iw in range(size_w):
                lbe =  ((1-τ)*grid_w[iw]*(grid_hT[t+1,iyp,iw] +  Σ*(1-grid_hT[t+1,iyp,iw])) )
                ep = np.hstack((np.array([lbe]), (1-τ)*grid_w[iw]*(grid_hT[t+1,iyp,iw]+Σ*(1-grid_hT[t+1,iyp,iw])) + (1+r_T[t+1])*grid_A[1:]))
                V_epT[t,iyp,:,iw] = np.interp( ep, grid_xe[iyp,:,iw],V_eT[t+1,iyp,:,iw] ) #, extrapolate = False, left = V_e[iyp,0,iw], right = V_e[iyp,-1,iw])
                d_Ve_prime[t,iyp,:,iw] = np.interp(ep,grid_xe[iyp,:,iw],dVe_T[t+1,iyp,:,iw])
                        
##########################################################
##########################################################
                                #Find the optimal market to search in, given the opportunity
        dηdθ = 1/((1+grid_θwT[t]**x)**(1/x)) - grid_θwT[t]**x/((1+grid_θwT[t]**x)**(1/x+1))
        dqdθ = dqdθ_fun(grid_θwT[t],x)
        
        for iy in range(size_y):
            transy = trans[iy]
            dθdw = -grid_qwT[t,iy]*(transy@dJT[t+1])/(dqdθ[iy]*(κ*(1+r_T[t+1])/grid_qwT[t,iy]))
            
            for ia in range(size_A):
                Vdiff =(trans[iy]@V_epT[t,:,ia]-trans[iy]@V_upT[t,:,ia])
                
                d_cont =- (dηdθ[iy]*dθdw*Vdiff+prob_etaT[t,iy]*(transy@d_Ve_prime[t,:,ia]))
                
                ss =1
                iw = -1
                while (ss >0) & (iw <size_w-3):
                    iw +=1
                    
                    if np.sign(d_cont[iw])!= np.sign(d_cont[iw+1]):
                        slope =  (d_cont[iw+1]-d_cont[iw])/(grid_w[iw+1]-grid_w[iw])
                        dw = -d_cont[iw]/slope
                        opt_w[iy,ia] = grid_w[iw]+dw
                        ss = -1
                    if iw == size_w-3:
                        opt_w[iy,ia] = grid_w[-1]
                        
##########################################################
##########################################################
                       
                
        for ia in range(1,size_A):
            xprime_u = (1-τ)*B + (1+r_T[t+1])*grid_A[ia]
            #Todays consumption, given tomorrows
            for iyp in range(size_y):
                xprime_e = (1-τ)*grid_w*(grid_hT[t+1,iyp] + Σ*(1-grid_hT[t+1,iyp]))  + (1+r_T[t+1])*grid_A[ia]
                
                
                cpu = linear.interp1d_scalar(xprime_u, np.hstack((np.array([0]), grid_xu[iyp])), np.hstack((np.array([0]), g_cu[iyp])))
                cprime_u[iyp]= cpu * (cpu<= xprime_u ) + xprime_u*(cpu>xprime_u )
                
                for iw in range(size_w):
                    #Resulting consumption
                    cpe = linear.interp1d_scalar(xprime_e[iw], np.hstack((np.array([0]), grid_xe[iyp, :, iw])), np.hstack((np.array([0]), g_ce[iyp, :, iw])))
                    cprime_e[iyp,iw] = cpe * (cpe<=xprime_e[iw] ) + xprime_e[iw]*(cpe>xprime_e[iw])
                                      
            
            for iy in range(size_y):
                transy = trans[iy]
                w1,w2 = basefun(grid_w,size_w,opt_w[iy,ia])
                cpe_int = w1[0]*cprime_e[:,w2[0]]+w1[1]*cprime_e[:,w2[1]]
                pe_int =  w1[0]*prob_etaT[t,iy,w2[0]]+w1[1]*prob_etaT[t,iy,w2[1]]

                #New consumption rules 
                g_cunew[iy,ia] =   ut.u_primeinv_s(β*(1+r_T[t+1])*((1-pe_int)*transy@ut.u_prime_v(cprime_u,γ) + \
                                                                      pe_int *transy@ut.u_prime_v(cpe_int,γ)),γ)       
                    
                for iw in range(size_w):
                     g_cenew[iy,ia,iw] =  ut.u_primeinv_s(β*(1+r_T[t+1])*((1-δ)*transy@ut.u_prime_v(cprime_e[:,iw],γ) + \
                                                                              δ*transy@ut.u_prime_v(cprime_u,γ)),γ)
                     
            #Recover new endogenous grid -- coh last period
            grid_xenew[:,ia,:]  = grid_A[ia] + g_cenew[:,ia,:]   
            grid_xunew[:,ia]    = grid_A[ia] + g_cunew[:,ia]  
    
        #Need to append the grids to include points on the x-grid that are possible
        #to reach but that entail a'=0
        grid_xunew[:,0] =np.ones((size_y))*lowbound
        g_cunew[:,0]    = np.ones((size_y))*lowbound
        
        for iw in range(size_w):
            grid_xenew[:,0,iw] = np.ones((size_y))*lowbound # ((1-τ)*grid_w[iw]*(grid_hT[t,:,iw] +  0.6*(1-grid_hT[t,:,iw])) + fp_T[t])
            g_cenew[:,0,iw] =    np.ones((size_y))*lowbound # ((1-τ)*grid_w[iw]*(grid_hT[t,:,iw] +  0.6*(1-grid_hT[t,:,iw])) + fp_T[t])
    
        #Value function tomorrow (prime) if a' was saved yesterday
        for iy in range(size_y):
            for ix in range(size_A):
                w1,w2 = basefun(grid_w,size_w,opt_w[iy,ix]) ##Which wage do unemployed choose on the grid?
                V_ep_int = w1[0]*V_epT[t,:,ix,w2[0]]+w1[1]*V_epT[t,:,ix,w2[1]]
                pe_int =  w1[0]*prob_etaT[t,iy,w2[0]]+w1[1]*prob_etaT[t,iy,w2[1]]
                
                transy = trans[iy]
                V_uT[t,iy,ix] = ut.u_scalar(g_cunew[iy,ix],γ) + β*((1-pe_int)*transy@V_upT[t,:,ix] + pe_int * transy@V_ep_int)
                dVe_T[t,iy,ix] = β*(1-δ)*transy@d_Ve_prime[t,:,ix] + (1-τ)*ut.u_prime_v(g_cenew[iy,ix],γ)*(grid_hT[t,iy] + Σ*(1-grid_hT[t,iy]))

                for iw in range(size_w):
                    V_eT[t,iy,ix, iw] = ut.u_scalar(g_cenew[iy,ix, iw],γ) +  β*(δ*transy@V_upT[t,:,ix] + (1-δ)*transy@V_epT[t,:,ix,iw])
        
        opt_wT[t] = opt_w
        grid_xeT[t]= grid_xenew
        grid_xuT[t]= grid_xunew
        g_ceT[t]= g_cenew
        g_cuT[t]= g_cunew
                
        grid_xenew, grid_xe = grid_xe, grid_xenew
        grid_xunew, grid_xu = grid_xu, grid_xunew
        
        g_cunew,g_cu = g_cu, g_cunew
        g_cenew,g_ce = g_ce, g_cenew  


    return opt_wT, grid_xeT, grid_xuT, g_ceT, g_cuT




@jit(nopython= True)  
def finegrids_trans(TT,grids, sizes,grid_xeT, grid_xuT, g_ceT, g_cuT, opt_wT,prob_etaT,grid_θwT,lowbound):
    size_y, size_w, size_a,size_afine,size_wfine = sizes
    _,grid_w, grid_a,_,trans,grid_afine,grid_wfine,grid_hfine = grids 
    
    g_puT = np.zeros((TT,size_y,size_a), dtype=np.float64)
    
    ####### Interpolate the policy functions on a finer grid
    #Employed (only assets), 2 state variables
    g_cefineT = np.zeros((TT,size_y,size_afine, size_wfine) , dtype = np.float64)
    grid_xefineT = np.zeros((TT,size_y,size_afine, size_wfine) , dtype = np.float64)
    
    g_wufineT = np.zeros((TT,size_y,size_afine), dtype=np.float64)
    g_pufineT = np.zeros((TT,size_y,size_afine), dtype=np.float64)
    
    #Finer cash-on-hand grid
    g_cufineT =    np.zeros((TT,size_y,size_afine) , dtype = np.float64)
    grid_xufineT = np.zeros((TT,size_y,size_afine) , dtype = np.float64)
    grid_θwfineT = np.zeros((TT,size_y,size_afine) , dtype = np.float64)

    for t in range(TT):
        
        for iy in range(size_y):
            for ia in range(size_a):
                w1,w2 = basefun(grid_w,size_w,opt_wT[t,iy,ia]) ##Which wage do unemployed choose on the grid?
                g_puT[t,iy,ia] = w1[0]*prob_etaT[t,iy,w2[0]]+w1[1]*prob_etaT[t,iy,w2[1]]
    
        for iy in range(size_y):
            #Interpolating decision rules onto finer grid
            grid_xufineT[t,iy]   = np.interp(grid_afine,grid_a,grid_xuT[t,iy])
            g_pufineT[t,iy,:]      = np.interp(grid_afine,grid_a,g_puT[t,iy]) ## Shouldn't be interpolated like this!
            g_wufineT[t,iy,:]      = np.interp(grid_afine,grid_a,opt_wT[t,iy]) ## Shouldn't be interpolated like this!
            grid_θwfineT[t,iy]        =  np.interp(grid_wfine, grid_w, grid_θwT[t,iy])
            
            for ia in range(size_afine):  

                for iw in range(size_wfine):
                    wval ,wind=  basefun(grid_w,size_w,grid_wfine[iw])
                    c0 = linear.interp1d_scalar(grid_afine[ia], grid_a, g_ceT[t, iy, :, wind[0]])
                    c1 = linear.interp1d_scalar(grid_afine[ia], grid_a, g_ceT[t, iy, :, wind[1]])
                    g_cefineT[t,iy,ia,iw] = wval[0]*c0 + wval[1]*c1
                    grid_xefineT[t,iy,ia,iw] = g_cefineT[t,iy,ia,iw] + grid_afine[ia]   
        
        #Need to append the grids to include points on the x-grid that are possible
        #to reach but that entail a'=0
        grid_xufineT[t,:,0] = np.ones((size_y))*lowbound
        g_cufineT[t,:,0] = np.ones(size_y)*lowbound
        
        for iw in range(size_wfine):
            grid_xefineT[t,:,0,iw] = np.ones((size_y))*lowbound # ((1-τ)*grid_w[iw]*(grid_h[:,iw] +  0.6*(1-grid_h[:,iw])))
            g_cefineT[t,:,0,iw] = np.ones(size_y)*lowbound #  ((1-τ)*grid_w[iw]*(grid_h[:,iw] +  0.6*(1-grid_h[:,iw])) ) 
               
    return grid_xefineT,grid_xufineT,g_cefineT,g_cufineT,g_pufineT,g_wufineT, grid_θwfineT      



## End of Period asset distribution
@jit(nopython= True)
def distfill_trans(TT,τ,greeks, sizes, grids, policiesT,r_T, dist_u,dist_e):
    β,_,γ,δ,Ɛ,κ,ξ,Σ,x,B,_,wprod,Λ = greeks

    size_y, _, _,size_afine,size_wfine = sizes
    _,_, _,_,trans,grid_afine,grid_wfine,_ = grids 
    
    dist_eT = np.zeros((TT,size_y,size_afine,size_wfine), dtype = np.float64)
    dist_uT = np.zeros((TT,size_y,size_afine), dtype = np.float64)
    
    grid_xefineT,grid_xufineT,_,_,g_pufineT,opt_wfineT,grid_hfineT = policiesT
    
    ## Fill FIRST distribution -- only r has changed --------------------------
    for ix in range(size_afine):   #Asset state
        
        #Enter the period with coh = ix -- find coh tomorrow
        ap = grid_afine[ix]   #Savings decision -- doesn't depend on y
       
        coh_uu = (1+r_T[0])*ap+(1-τ)*B #doesn't depend on y for unemployed
        for iy in range(size_y):
            assigu = dist_u[iy,ix]
      

            w1,w2 = basefun(grid_wfine,size_wfine,opt_wfineT[0,iy,ix])
            ηy = g_pufineT[0,iy,ix]
            
            for iyp in range(size_y):
                vals_uu, inds_uu = basefun(grid_xufineT[0,iyp],size_afine,coh_uu)

                ### Tomorrow's COH if one finds employment
                coh_ue0 = (1+r_T[0])*ap+(1-τ)*grid_wfine[w2[0]]*(grid_hfineT[0,iyp,w2[0]]+Σ*(1-grid_hfineT[0,iyp,w2[0]]))
                vals_ue0, inds_ue0 = basefun(grid_xefineT[0,iyp,:,w2[0]],size_afine,coh_ue0)
                
                coh_ue1 = (1+r_T[0])*ap+(1-τ)*grid_wfine[w2[1]]*(grid_hfineT[0,iyp,w2[1]]+Σ*(1-grid_hfineT[0,iyp,w2[1]]))
                vals_ue1, inds_ue1 = basefun(grid_xefineT[0,iyp,:,w2[1]],size_afine,coh_ue1)


                tu = trans[iy,iyp]
                ### U2U transitions
                dist_uT[0,iyp,inds_uu[0]] += tu*assigu*vals_uu[0]*(1-ηy) 
                dist_uT[0,iyp,inds_uu[1]] += tu*assigu*vals_uu[1]*(1-ηy)         

                ### U2E transitions 
                dist_eT[0,iyp,inds_ue0[0],w2[0]] += tu*assigu*vals_ue0[0]*ηy*w1[0]
                dist_eT[0,iyp,inds_ue0[1],w2[0]] += tu*assigu*vals_ue0[1]*ηy*w1[0]
                
                dist_eT[0,iyp,inds_ue1[0],w2[1]] += tu*assigu*vals_ue1[0]*ηy*w1[1]
                dist_eT[0,iyp,inds_ue1[1],w2[1]] += tu*assigu*vals_ue1[1]*ηy*w1[1]
                
            for iw in range(size_wfine):   #Wage state
                assige=dist_e[iy,ix,iw]

                
                for iyp in range(size_y):
                    
                    #Tomorrow's COH if one stays employed
                    coh_ee = (1+r_T[0])*ap+(1-τ)*grid_wfine[iw]*(grid_hfineT[0,iyp,iw] +Σ*(1-grid_hfineT[0,iyp,iw]))
                    valse, indse = basefun(grid_xefineT[0,iyp,:,iw],size_afine,coh_ee)
    
                    #Tomorrow's COH if one becomes unemployed
                    coh_eu = (1+r_T[0])*ap+(1-τ)*B
                    valsu, indsu = basefun(grid_xufineT[0,iyp],size_afine,coh_eu)  
                    
                    te =  trans[iy,iyp]
                    ### E2E transitions
                    dist_eT[0,iyp,indse[0],iw] += te*assige*valse[0]*(1-δ)
                    dist_eT[0,iyp,indse[1],iw] += te*assige*valse[1]*(1-δ)
                    
                    ### E2U transitions
                    dist_uT[0,iyp,indsu[0]] +=  te*assige*valsu[0]*δ
                    dist_uT[0,iyp,indsu[1]] +=  te*assige*valsu[1]*δ   
    
    
    
    for t in range(TT-1):
        for ix in range(size_afine):   #Asset state
            
            #Enter the period with coh = ix -- find coh tomorrow
            ap = grid_afine[ix]   #Savings decision -- doesn't depend on y

            coh_uu = (1+r_T[t+1])*ap+(1-τ)*B #doesn't depend on y for unemployed
            for iy in range(size_y):
                
                #Mass to be assigned
                assigu = dist_uT[t,iy,ix]
                
                w1,w2 = basefun(grid_wfine,size_wfine,opt_wfineT[t+1,iy,ix])
                ηy = g_pufineT[t+1,iy,ix]       
                
      
                for iyp in range(size_y):
                    vals_uu, inds_uu = basefun(grid_xufineT[t+1,iyp,:],size_afine,coh_uu)
                    
                    ### Tomorrow's COH if one finds employment
                    coh_ue0 = (1+r_T[t+1])*ap+(1-τ)*grid_wfine[w2[0]]*(grid_hfineT[t+1,iyp,w2[0]]+Σ*(1-grid_hfineT[t+1,iyp,w2[0]]))
                    vals_ue0, inds_ue0 = basefun(grid_xefineT[t+1,iyp,:,w2[0]],size_afine,coh_ue0)
                    
                    coh_ue1 = (1+r_T[t+1])*ap+(1-τ)*grid_wfine[w2[1]]*(grid_hfineT[t+1,iyp,w2[1]]+Σ*(1-grid_hfineT[t+1,iyp,w2[1]]))
                    vals_ue1, inds_ue1 = basefun(grid_xefineT[t+1,iyp,:,w2[1]],size_afine,coh_ue1)
              
                    
                    tu = trans[iy,iyp]
                    ### U2U transitions
                    dist_uT[t+1,iyp,inds_uu[0]] += tu*assigu*vals_uu[0]*(1-ηy) 
                    dist_uT[t+1,iyp,inds_uu[1]] += tu*assigu*vals_uu[1]*(1-ηy)         
    
                    ### U2E transitions 
                    dist_eT[t+1,iyp,inds_ue0[0],w2[0]] += tu*assigu*vals_ue0[0]*ηy*w1[0]
                    dist_eT[t+1,iyp,inds_ue0[1],w2[0]] += tu*assigu*vals_ue0[1]*ηy*w1[0]
                    
                    dist_eT[t+1,iyp,inds_ue1[0],w2[1]] += tu*assigu*vals_ue1[0]*ηy*w1[1]
                    dist_eT[t+1,iyp,inds_ue1[1],w2[1]] += tu*assigu*vals_ue1[1]*ηy*w1[1]
            
            
                for iw in range(size_wfine):   #Wage state
                    assige=dist_eT[t,iy,ix,iw]

                    
                    for iyp in range(size_y):
                        #Tomorrow's COH if one stays employed
                        coh_ee = (1+r_T[t+1])*ap+(1-τ)*grid_wfine[iw]*(grid_hfineT[t+1,iyp,iw] +Σ*(1-grid_hfineT[t+1,iyp,iw]))
                        valse, indse = basefun(grid_xefineT[t+1,iyp,:,iw],size_afine,coh_ee)
        
                        #Tomorrow's COH if one becomes unemployed
                        coh_eu = (1+r_T[t+1])*ap+(1-τ)*B
                        valsu, indsu = basefun(grid_xufineT[t+1,iyp,:],size_afine,coh_eu)  
                        
                        te =  trans[iy,iyp]
                        ### E2E transitions
                        dist_eT[t+1,iyp,indse[0],iw] += te*assige*valse[0]*(1-δ)
                        dist_eT[t+1,iyp,indse[1],iw] += te*assige*valse[1]*(1-δ)
                        
                        ### E2U transitions
                        dist_uT[t+1,iyp,indsu[0]] +=  te*assige*valsu[0]*δ
                        dist_uT[t+1,iyp,indsu[1]] +=  te*assige*valsu[1]*δ

    return dist_uT, dist_eT





