# -*- coding: utf-8 -*-
"""
Created on Fri Feb 14 10:22:46 2020

@author: jokr4584
"""

import numpy as np
import matplotlib.pyplot as plt

from pydynopt.processes import tauchen, markov_ergodic_dist

#User written functions

#Own functions
#from ds_fun import cons_egm
from ds_fun_noa import cons_egmA

#This is an attempt to solve the steady state of a directed search model
###############################################################################
######################## #Parameters
#parameters
β = 0.97
r = .04

#Risk aversion -- Parameter for CRRA utility!
γ = 2
#Exogenous separation probability
δ = .05
#Vacancy posting cost
κ = .03
#Parameter in the matching function
x = .3
#Unemployment benefits
B = .4
#Labor tax used to finance bond issuance and benefits
τ = 0.2
#Elasticity of substitution across varieties
Ɛ = 6
#Parameter in the STW cost function
ξ = 2
#Parameter governing likelihood of search OTJ
Λ = 0
#STW replacement rate
Σ = 0.6

#Productivity cost 
wprod = (Ɛ-1)/Ɛ

###############################################################################
################ GRIDS
#Tightness vector
size_θ = 1000
grid_θ = np.linspace(0.0001,50,size_θ, dtype=np.float64)

#Aggregate productivity is fixed for now, there is only match productivity
y_var = 0.3**2
y_rho = .8
size_y = 3

grid_y, trans, *rest = tauchen(y_rho, np.sqrt(y_var), n=size_y)
grid_y = np.exp(grid_y)
yss = markov_ergodic_dist(trans, inverse=True)
# trans = qe.markov.approximation.tauchen(y_rho,y_var,3,size_y).P
# grid_y = np.exp(qe.markov.approximation.tauchen(y_rho,y_var,3,size_y)._state_values)
#Normalize back to total productivity = 1
# yss = solveStationary(trans)
grid_y = grid_y/(yss@grid_y)


#Grid for COH
############## Asset grid for distribution
a1 = 0.0000001
afin = 50

size_a = 50 #number of gridpoints on A
#size_xfine = 500 #number of gridpoints on A
grid_a = np.hstack((np.array([0.0,0.0]),np.logspace(np.log(a1),np.log(afin),size_a-2,endpoint=True, base = np.exp(1), dtype=np.float64)))
#grid_a =np.linspace(a1, afin, size_a, dtype=np.float64)
size_afine = 150
grid_afine = np.hstack((np.array([0.0,0.0]),np.logspace(np.log(a1),np.log(afin),size_afine-2,endpoint=True, base = np.exp(1), dtype=np.float64)))

###############################################################################
#Wage vector
size_w = 99
size_wfine = 150
wfin = 1.4 #np.max(grid_y)
grid_w = np.linspace(B,wfin,size_w, dtype = np.float64)
grid_wfine =  np.linspace(B,wfin,size_wfine, dtype = np.float64)
mat_w = np.tile(grid_w,(size_y,1))


###############################################################################
############## Firm Problem --- Short Time Work

#### Grids
sizes = size_y, size_w, size_a,size_afine,size_wfine
grids = grid_y,grid_w, grid_a,trans,grid_afine,grid_wfine
   
###############################################################################
###############################################################################
class Container:
    def __init__(self):
        self.fpguess =99
        self.tauguess = .1
        self.rguess = 0.01
        self.prob_eta = np.zeros((size_y,size_w), dtype=np.float64)
        
        
container = Container()
container.fpguess =0.06
rguess = r
container.tauguess =  τ
bond =1

#r = 0.05
greeks = β,r,γ,δ,Ɛ,κ,ξ,Σ,x,B,bond,wprod,0

grid_xe, grid_xu, g_ce, g_cu, opt_w,_,_,_,_,_,prob_eta,grid_θw   = cons_egmA(container.tauguess,sizes, grids, greeks)


plt.plot(np.transpose(grid_xu), np.transpose(opt_w))
plt.plot(np.transpose(grid_xu), np.transpose(opt_w2))

rn = 1/grid_θw**x * (grid_w-B)/(1-trans@(β*(1-δ-prob_eta)))-κ*(1+r)/grid_qw
rnvec=np.zeros((3,size_a))
rnvec[0]=np.interp(0,rn[0],grid_w)*np.ones(size_a)
rnvec[1]=np.interp(0,rn[1],grid_w)*np.ones(size_a)
rnvec[2]=np.interp(0,rn[2],grid_w)*np.ones(size_a)
plt.plot(np.transpose(grid_xu),np.transpose(rnvec))






